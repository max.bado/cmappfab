function New-CMAFApplication {
    [CmdletBinding()]
    Param(
        $Name,
        $Publisher,
        $SoftwareVersion,
        $Title,
        $ReleaseDate,
        $AutoInstall = $true,
        [Parameter(Mandatory = $false)]
        $AppDT,
        $appFriendlyName, ##
        [Parameter(Mandatory = $false)]
        [string]$AppSoftwareVersion,
        [Parameter(Mandatory = $false)]
        [string]$CMServer,
        [Parameter(Mandatory = $false)]
        [string]$CMSite,
        [Parameter(Mandatory = $false)]
        $CMAppList,
        [Parameter(Mandatory = $false)]
        [string]$XMLTemplateDirectoryPath = "$PSScriptRoot\XMLs",
        $appDTCol,
        $AppFab
        
    )

    # Initialize application factory
    $Factory = [Microsoft.ConfigurationManagement.AdminConsole.AppManFoundation.ApplicationFactory]::new()

    # Initialize wrapper
    $Wrapper = [Microsoft.ConfigurationManagement.AdminConsole.AppManFoundation.AppManWrapper]::Create($ConnectionManager, $Factory)

    # Create Application object
    $Application = [Microsoft.ConfigurationManagement.ApplicationManagement.Application]::new()
    $Application.Title = $AppFab.Name
    $Application.Publisher = $AppFab.Publisher
    $Application.SoftwareVersion = $AppFab.SoftwareVersion
    $Application.ReleaseDate = $AppFab.ReleaseDate


    # Create the Display Info list object
    $AppDisplayInfo = [Microsoft.ConfigurationManagement.ApplicationManagement.AppDisplayInfo]::new()
    $AppDisplayInfo.Title = $AppFab.LocalizedName
    #$AppDisplayInfo.Description = 'Mozilla Firefox ESR for Labs'
    $AppDisplayInfo.Language = 'EN'
    $AppDisplayInfo.Publisher = $AppFab.Publisher
    $AppDisplayInfo.Version = $AppFab.SoftwareVersion
    $AppDisplayInfo.ReleaseDate = $AppFab.ReleaseDate

    # Icon
    $AppDisplayInfo.Icon = $AppFab.Icon

    # Other Application Details
    $Application.AutoInstall = $AppFab.AutoInstall
    $Application.Recommended = $false

    # Add owner
    $Owner = [Microsoft.ConfigurationManagement.ApplicationManagement.User]::new()
    $Owner.Id = $env:USERNAME
    $Owner.Qualifier = 'LogonName'
    $null = $Application.Owners.Add($Owner)

    # Add contacts
    $Owner.Qualifier = 'LogonName'
    $null = $Application.Contacts.Add($Owner)

    # Other Application Details
    $Application.HighPriority = 2
    $Application.AutoDistribute = $true

    # Deployment Types
    foreach ($DeploymentType in $AppFab.DeploymentTypes) {

        $ScriptInstaller = [Microsoft.ConfigurationManagement.ApplicationManagement.ScriptInstaller]::new()
        $ScriptInstaller.InstallCommandLine = $DeploymentType.InstallCommand
        $ScriptInstaller.UninstallCommandLine = $DeploymentType.UninstallCommand
        $ScriptInstaller.RepairCommandLine = $DeploymentType.RepairCommand
        $ScriptInstaller.AllowUninstall = $true

        ## Detection Script
        $DetectionScript = [Microsoft.ConfigurationManagement.ApplicationManagement.Script]::new()
        $DetectionScript.Language = $DeploymentType.ScriptLanguage
        $DetectionScript.Text = $DeploymentType.ScriptText

        $ScriptInstaller.DetectionScript = $DetectionScript

        ## Other ScriptInstaller properties
        $ScriptInstaller.UserInteractionMode = [Microsoft.ConfigurationManagement.ApplicationManagement.UserInteractionMode]::Hidden
        $ScriptInstaller.PostInstallBehavior = [Microsoft.ConfigurationManagement.ApplicationManagement.PostExecutionBehavior]::NoAction
        Write-Host "ExecutionContext $($DeploymentType.ExecutionContext)"
        $ScriptInstaller.ExecutionContext = [Microsoft.ConfigurationManagement.ApplicationManagement.ExecutionContext]::"$($DeploymentType.ExecutionContext)"

        ## Content
        # Resolve content path
        $ContentPath = $null
        $ContentPath = Get-Item -Path $DeploymentType.ContentLocation
        Write-Host "Content: $($ContentPath.FullName)"

        $Content = [Microsoft.ConfigurationManagement.ApplicationManagement.ContentImporter]::CreateContentFromFolder("$($ContentPath.FullName)")
        $null = $ScriptInstaller.Contents.Add($Content)

        ## Content Ref
        $ContentRef = [Microsoft.ConfigurationManagement.ApplicationManagement.ContentRef]::new()
        $ContentRef.Id = $Content.Id
        $ScriptInstaller.InstallContent = $ContentRef
        $ScriptInstaller.UninstallContent = $ContentRef

        ## Create Deployment Type
        $DT = [Microsoft.ConfigurationManagement.ApplicationManagement.DeploymentType]::new($ScriptInstaller, [Microsoft.ConfigurationManagement.ApplicationManagement.ScriptInstaller]::TechnologyId, [Microsoft.ConfigurationManagement.ApplicationManagement.NativeHostingTechnology]::TechnologyId)
        $DT.Title = $DeploymentType.DeploymentTypeName

        
        ## Dependencies
        if ($DeploymentType.Dependencies) {
            foreach ($DTDep in $DeploymentType.Dependencies) {
                $null = $DT.Dependencies.Add($DTDep)
            }            
        }
        #>
        
        ## Requirements
        if ($DeploymentType.Requirements) {
            foreach ($DTReq in $DeploymentType.Requirements) {
                $null = $DT.Requirements.Add($DTReq)
            }
        }
        #>

        ## Supersedence
        if ($DeploymentType.Supersedes) {
            foreach ($DTSup in $DeploymentType.Supersedes) {
                $null = $DT.Supersedes.Add($DTSup)
            }
        }

        ## Add Deployment Type
        $null = $Application.DeploymentTypes.Add($DT)
    }


    # User Categories
    if ($AppFab.UserCategories) {
        foreach ($UserCategory in $AppFab.UserCategories.Category) {            
            $null = $AppDisplayInfo.UserCategories.Add(($UserCategories | Where-Object {$_.LocalizedCategoryInstanceName -eq $UserCategory}).CategoryInstance_UniqueID)
        }
    }

    # App Categories
    if ($AppFab.AppCategories) {
        $AppCategoryList = @()
        $AppCategoryList = foreach ($AppCategory in $AppFab.AppCategories.Category) {
            ($AppCategories | Where-Object {$_.LocalizedCategoryInstanceName -eq $AppCategory}).CategoryInstance_UniqueID            
        }

        $Wrapper.CategoryIds = $AppCategoryList
    }

    # Add the Display Info object to the app
    $null = $Application.DisplayInfo.Add($AppDisplayInfo)

    # Wrap, prepare, and create the app
    #[Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::SerializeToString($Application) | Out-File -FilePath "C:\Users\mbado\Desktop\$($Application.Title).xml" -Force
    $Wrapper.InnerAppManObject = $Application
    $Factory.PrepareResultObject($Wrapper)
    $null = $Wrapper.InnerResultObject.Put()
}