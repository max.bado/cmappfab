#region ------------------------------------------------[Parse-VersionString]------------------------------------------------
function Parse-VersionString {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        [string]$VersionString,
        [Parameter(Mandatory = $false)]
        [switch]$AsChar
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    Write-Host "***********************************************" -ForegroundColor Blue -BackgroundColor Black
    Write-Host "VersionString: $VersionString" -ForegroundColor Blue -BackgroundColor Black

    $ParseType = 'int'
    try {
        #$versionVersion_Current = [version]$App_VersionPathLeaf
        $VersionVersion = [version]$VersionString
    } catch {
        # Exception most likely occured while trying to cast a non-[version]-like version string...
        $Exception = $_.Exception
        if ($Exception -like '*Version string portion was too short or too long*') {
            # Checking if the version string was a whole number...
            if ([int]::TryParse($VersionString, [ref]$null)) {
                # Append a .0 if the version string is a whole number
                $VersionVersion = [version]"$($VersionString).0"
            } else {
                $_
                #Throw "$VersionString does not appear to be an [int]. Stopping."
                return
            }
        } else {
            # The version string probably has some non-integer characters in it...
            Write-Host "Couldn't convert $VersionString into a [Version] object..." -ForegroundColor Yellow -BackgroundColor Black
            Write-Host "We're going to try to break apart the version and convert the individual parts into base 10 integers." -ForegroundColor Yellow -BackgroundColor Black
            $VersionSplit = $VersionString.Split('.')
            if (($VersionSplit | Measure-Object).Count -gt 1) {
                # Making sure that the version string has more than one part separated by a period... Not sure if this check needs to exist...    
                $VersionConvertedArray = $null
                try {
                    $VersionPartIndex = 0
                    $VersionConvertedArray = foreach ($VersionPart in $VersionSplit) {
                        # Parse each part of the version string separately and combine them in the VersionConvertedArray
                        Write-Host "    VersionPart: $VersionPart" -ForegroundColor Blue -BackgroundColor Black
                        $ConvertedPart = $null

                        if (($VersionPartIndex -eq 0) -and !([int]::TryParse($VersionPart, [ref]$null))) {
                            # If the first part of the version is not an [int], end parsing this version string
                            Write-Host ("    $VersionPart is the first section of the version string but is not an [int]. " + 
                                "End parsing this version string.") -ForegroundColor Red -BackgroundColor Black
                            #Throw "$VersionPart is the first section of the version string but is not an [int]."
                            return
                        }

                        if ([int]::TryParse($VersionPart, [ref]$null)) {
                            # If the part is an int...
                            Write-Host "    $VersionPart appears to be a standard base 10 integer." -ForegroundColor Green -BackgroundColor Black
                            $ConvertedPart = $VersionPart                             
                        } else {
                            # If the part is not an int...
                            if (!$AsChar) {
                                try {
                                    # Check if it's a hexadecimal...
                                    $ConvertedPart = [Convert]::ToInt64($VersionPart, 16)
                                    Write-Host "    $VersionPart appears to be a hexadecimal representation of $ConvertedPart." -ForegroundColor Green -BackgroundColor Black
                                    $ParseType = 'hex'
                                } catch {
                                    # Not a hexadecimal, take the [char] sum of the part
                                    Write-Host "    Couldn't convert $VersionPart from hex to decimal. Trying to convert to numerical sum of the [chars]." -ForegroundColor Yellow -BackgroundColor Black
                                    $ConvertedPart = ($VersionPart.ToCharArray() | ForEach-Object {[int]$_} | Measure-Object -Sum).Sum                                    
                                    Write-Host "    The [char] sum representation of $VersionPart is $ConvertedPart." -ForegroundColor Green -BackgroundColor Black
                                    $ConvertedPart
                                    $ParseType = 'char'
                                }
                            } else {
                                # AsChar switch specified, parsing the VersionPart as the sum of [char]s
                                $ConvertedPart = ($VersionPart.ToCharArray() | ForEach-Object {[int]$_} | Measure-Object -Sum).Sum                                    
                                Write-Host "    The [char] sum representation of $VersionPart is $ConvertedPart." -ForegroundColor Green -BackgroundColor Black
                                $ConvertedPart
                                $ParseType = 'char'
                            }
                        }
                        
                        # Incremenent the index
                        $VersionPartIndex++

                        $VersionPartObject = [pscustomobject]@{
                            VersionPart = $ConvertedPart
                            ParseType   = $ParseType
                        }

                        $VersionPartObject
                    }
                } catch {                        
                    $_
                    #Throw "Couldn't convert $VersionString into a [Version] object even after converting offending parts from hex to decimal. Stopping."
                    return
                }
            } else {
                Write-Host "$VersionString doesn't appear to be a [Version]." -ForegroundColor Red -BackgroundColor Black
            }

            $VersionConvertedString = $VersionConvertedArray.VersionPart -join '.'
            $VersionVersion = [version]$VersionConvertedString
            #$stringVersion_Current = $App_VersionPathLeaf

            Write-Host "For the purpose of version comparison, $VersionString will be $VersionConvertedString." -ForegroundColor Green -BackgroundColor Black
        }   
    }

    if ($VersionConvertedArray.ParseType -contains 'char') {
        $ParseType = 'char'
    } elseif ($VersionConvertedArray.ParseType -contains 'hex') {
        $ParseType = 'char'
    } else {
        $ParseType = 'int'
    }

    $ParsedVersionObject = [pscustomobject]@{
        Version   = $VersionVersion
        String    = $VersionString
        ParseType = $ParseType
    }

    Write-Host "END $CmdletName" -ForegroundColor Green

    $ParsedVersionObject
}
#endregion

#region ------------------------------------------------[Get-CMAFApplicationUpdatedVersions]------------------------------------------------
function Get-CMAFApplicationUpdatedVersions {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $false)]
        [string]$CMServer,
        [Parameter(Mandatory = $false)]
        [string]$CMSite,
        [Parameter(Mandatory = $true, ParameterSetName = 'TemplatePath')]
        [string[]]$AppTemplatePath,
        [Parameter(Mandatory = $true, ParameterSetName = 'AppName')]
        [string[]]$ApplicationName,
        [Parameter(Mandatory = $true, ParameterSetName = 'AppName')]
        [string]$AppTemplateDirectoryPath,
        [Parameter(Mandatory = $false)]
        $CMAppList,
        [Parameter(Mandatory = $false)]
        [switch]$DisableCurrentApplicationCheck,
        [Parameter(Mandatory = $false)]
        [string]$LogFile = $null
    )
    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    # If CMServer or CMSite were not specified, retrieve the info from the Microsoft.SMS.Client object 
    if (!($Global:CMServer) -or !($Global:CMSite)) {        
        $SMSClientObject = New-Object -ComObject Microsoft.SMS.Client -Strict
        if (!$CMServer) {
            Write-Host "Setting CMServer"
            $CMServer = $SMSClientObject.GetCurrentManagementPoint()
        }
        if (!$CMSite) {
            Write-Host "Setting CMSite"
            $CMSite = $SMSClientObject.GetAssignedSite()
        }
    } else {
        $CMServer = $Global:CMServer
        $CMSite = $Global:CMSite
    }

    # If CMAppList was not passed as a parameter or the variable does not exist in this scope, retrieve it
    if (!$Global:CMAppList) {
        Write-Host "Retrieving CM Application List."
        $CMAppListQuery = "SELECT * FROM SMS_Application
            WHERE SMS_Application.IsLatest = 'True' 
            AND SMS_Application.IsEnabled = 'True' 
            AND SMS_Application.IsExpired = 'False'"
    
        $Global:CMAppList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query $CMAppListQuery
    } else {
        $CMAppList = $Global:CMAppList
    }

    [System.Collections.ArrayList]$Apps_ToBeUpdated = @()

    Write-Host "ParameterSetName: $($PSCmdlet.ParameterSetName)"

    if($PSCmdlet.ParameterSetName -eq 'TemplatePath') {
        $ApplicationObjectList = foreach($AppTemplate in $AppTemplatePath) {            
            $ApplicationObject = [pscustomobject]@{
                ApplicationName = (Get-Item -Path $AppTemplate).BaseName
                AppXMLContent = Get-Content -Path $AppTemplate
            }

            $ApplicationObject            
        }
    } else {
        $ApplicationObjectList = foreach($App in $ApplicationName) {
            $ApplicationObject = [pscustomobject]@{
                ApplicationName = $App
                AppXMLContent = Get-Content -Path "$AppTemplateDirectoryPath\$App.xml"
            }

            $ApplicationObject
        }
    }

    if (!$LogFile) {
        if($ApplicationObjectList.Count -gt 1) {
            $LogFile = "$PSScriptRoot\Logs\$($CmdletName)_MultipleApps_$(Get-Date -Format yyyy-mm-ddThh-mm).log"
        } else {
            $LogFile = "$PSScriptRoot\Logs\$($CmdletName)_$($ApplicationObjectList.ApplicationName).log"
        }
    }

    Write-Host "Log File located at: $LogFile"

    foreach ($ApplicationObject in $ApplicationObjectList) {
        # Get App XML Template
        #$AppXMLTemplatePath = (Get-Item -Path $AppTemplate).FullName
        #$AppXMLTemplate = Get-Item -Path $AppXMLTemplatePath
        #$AppTemplateName = $AppXMLTemplate.BaseName
        $AppTemplateName = $ApplicationObject.ApplicationName
        #$AppXMLContent = (Get-Content -Path $AppXMLTemplatePath)
        $AppXMLContent = $ApplicationObject.AppXMLContent

        <#
        if (!$AppXMLTemplate) {
            Write-Error -Message "$App does not have a corresponding XML Template."
            break
        }
        #>

        # Convert content to XML
        $appXML = [xml]$appXMLContent

        # This will be used in case no previous versions of the app exist
        $appDTContentLocation = $appXML.Application.DeploymentType.ContentLocation | Select-Object -First 1

        # Make special consideration for Applications with a suffix such as 'for Faculty and Staff' or 'for Labs', etc.
        $appNameSuffix = $appXML.Application.DisplayInfo.Suffix

        $CMApp = $null
        if ($appNameSuffix) {
            $appBaseName = ($appTemplateName.Replace($appNameSuffix, '')).Trim()
            #$appBaseName = $appBaseName.Remove($appBaseName.Length - 1)
            #$appName = "$appBaseName $appSoftwareVersion $appNameSuffix"
            
            # If the application name has a suffix, check for related apps to exclude
            $RelatedCMAppList = $null
            $RelatedCMAppList = $CMAppList | Where-Object {($_.LocalizedDisplayName -match $AppBaseName) -and ($_.LocalizedDisplayName -notmatch $appNameSuffix)}
            if(!$DisableCurrentApplicationCheck) {
                $CMApp = $CMAppList | Where-Object {($_.LocalizedDisplayName -like "$AppBaseName*") -and ($_ -notin $RelatedCMAppList)} | Sort-Object -Property DateCreated -Descending | Select-Object -First 1
            } else {
                $CMApp = $null
            }
        } else {
            $appBaseName = $appTemplateName
            #$appName = "$appBaseName $appSoftwareVersion"    
            if(!$DisableCurrentApplicationCheck) {        
                $CMApp = $CMAppList | Where-Object {$_.LocalizedDisplayName -like "$AppBaseName*"} | Sort-Object -Property DateCreated -Descending | Select-Object -First 1
                Write-Host "CMApp: $($CMApp.LocalizedDisplayName)"
            } else {
                $CMApp = $null
            }
        }

        $App_WMIApplication = $null
        $App_DeserializedXML = $null
        $App_DeploymentTypes = $null
        $AppDT_ContentLocation = $null
        $App_VersionPath = $null
        $App_VersionPathLeaf = $null
        $App_ArchivePath = $null
        $App_ArchiveItemList = $null
        $App_ArchiveItemsLatest = $null
        $versionVersion_Current = $null
        $stringVersion_Current = $null
        $versionVersion_Latest = $null
        $stringVersion_Latest = $null
        $App_VersionPathLatest = $null
        $ParseAsChar = $false
        $ReparseAsChar = $false
        $CurrentParseType = $null

        if ($CMApp) {
            # If versions of the application already exist, pull information from the latest version
            #$App_WMIApplication = [wmi]$CMApp.__PATH
            $App_WMIApplication = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($CMApp.CI_ID)"
            $App_DeserializedXML = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($App_WMIApplication.SDMPackageXML)
    
            $App_Version = $App_DeserializedXML.SoftwareVersion
            #$Version_Current = $App_Version
            $App_DeploymentTypes = $App_DeserializedXML.DeploymentTypes
            $AppDT_ContentLocation = $App_DeploymentTypes[0].Installer.Contents[0].Location
    
            # Get the location of the latest version of the application
            $App_VersionPath = Split-Path -Path $AppDT_ContentLocation -Parent
            $App_ArchivePath = Split-Path -Path $App_VersionPath -Parent

            Write-Host ''
            Write-Host "Working on " -NoNewline
            Write-Host "$($CMApp.LocalizedDisplayName)" -ForegroundColor Magenta
            Write-Host "Previous version of application exists... "

        } else {
            # If no versions of the application exist yet, get information from the XML template
            $AppDT_ContentLocation = $appDTContentLocation

            # Get the root location of the application we're working on
            $App_VersionPath = Split-Path -Path $AppDT_ContentLocation -Parent
            $App_ArchivePath = Split-Path -Path $App_VersionPath -Parent

            Write-Host ''
            Write-Host "Working on " -NoNewline
            Write-Host $appBaseName -ForegroundColor Magenta
        }

        # Enumerate version directories in the Archive Path
        $App_ArchiveItemList = Get-ChildItem -Path $App_ArchivePath -Directory
        
        ## START Current Version parsing
        if ($appBaseName -like 'Adobe Acrobat*') {
            # Special consideration for Adobe Acrobat due to structure of the version archive
            $versionVersion_Current = [version]$App_Version
            $stringVersion_Current = $App_Version
        } else {
            # Get the version number from the path name
            $App_VersionPathLeaf = Split-Path -Path $App_VersionPath -Leaf
            Write-Host "App_VersionPath: $App_VersionPath"
            Write-Host "App_VersionPathLeaf: $App_VersionPathLeaf"

            if ($App_VersionPathLeaf -eq '{SoftwareVersion}') {
                # This is an application that does not have previous versions. Setting current version to 0.0
                Write-Host "$App_VersionPathLeaf -eq '{SoftwareVersion}'"
                Write-Host "This is an application that does not have previous versions. Setting current version to 0.0"
                $versionVersion_Current = [version]'0.0'
                $stringVersion_Current = '0.0'
            } else {
                # This application has previous versions
                Write-Host "$App_VersionPathLeaf -ne '{SoftwareVersion}'"
                Write-Host "This application has previous versions. Parsing the version number from the directory name."
                $versionVersion_CurrentObject = Parse-VersionString -VersionString $App_VersionPathLeaf
                $versionVersion_Current = $versionVersion_CurrentObject.Version
                $stringVersion_Current = $App_VersionPathLeaf
                $CurrentParseType = $versionVersion_CurrentObject.ParseType
                if ($CurrentParseType -eq 'char') {
                    $ParseAsChar = $true
                }
            }
        }    

        $versionVersion_Latest = $versionVersion_Current
        $stringVersion_Latest = $stringVersion_Current
        ## END Current Version parsing

        ## START parsing Version Items in Archive Directory
        $Version_ArchiveItemObjectList = foreach ($App_ArchiveItem in $App_ArchiveItemList) {
            $Version_ArchiveItemObject = $null
            if ($CMApp.LocalizedDisplayName -like 'Google Chrome*') {
                if (!(Test-Path -Path "$($App_ArchiveItem.FullName)\win_x64" -ErrorAction SilentlyContinue)) {
                    Write-Host "The $($App_ArchiveItem.Name) version of chrome is x86 architecture, not x64." -ForegroundColor Red
                    continue
                }
            }

            $Version_ArchiveItemObject = Parse-VersionString -VersionString $App_ArchiveItem.Name -AsChar:$ParseAsChar
            if (!$Version_ArchiveItemObject) {
                # Parse-VersionString returned a $null. Move to next version item
                continue
            }
            
            $NewParseType = $Version_ArchiveItemObject.ParseType
            if (($NewParseType -eq 'char') -and ($CurrentParseType -ne 'char')) {
                # While parsing the the version archive, one of the versions was parsed as the sum of [char]s, but the previous
                # version items were not. This means we have to reparse each item as the sum of [char]s to have a consistent comparison
                Write-Host ("The version string $($App_ArchiveItem.Name) was parsed as the sum of [char]s, $($Version_ArchiveItemObject.Version). " + 
                    "This means we have to reparse every version in the archive as [char]s. Setting ReparseAsChar and ParseAsChar flags.") -ForegroundColor Red
                $ReparseAsChar = $true
                $ParseAsChar = $true
                break
            }

            # Add the Archive Path item to the Archive Version Object
            $null = Add-Member -InputObject $Version_ArchiveItemObject -MemberType NoteProperty -Name 'App_ArchiveItem' -Value $App_ArchiveItem
            $Version_ArchiveItemObject
        }

        # If it is determined that the versions need to be parsed as [char]s
        if ($ReparseAsChar) {
            Write-Host "Reparsing each version in the archive as [char]s." -ForegroundColor Red -BackgroundColor Yellow
            $Version_ArchiveItemObjectList = foreach ($App_ArchiveItem in $App_ArchiveItemList) {
                $Version_ArchiveItemObject = $null    
                if ($CMApp.LocalizedDisplayName -like 'Google Chrome*') {
                    if (!(Test-Path -Path "$($App_ArchiveItem.FullName)\win_x64" -ErrorAction SilentlyContinue)) {
                        Write-Host "The $($App_ArchiveItem.Name) version of chrome is x86 architecture, not x64." -ForegroundColor Red
                        continue
                    }
                }    
                
                $Version_ArchiveItemObject = Parse-VersionString -VersionString $App_ArchiveItem.Name -AsChar:$ParseAsChar    
                if (!$Version_ArchiveItemObject) {
                    # Parse-VersionString returned a $null. Move to next version item
                    continue
                }
                # Add the Archive Path item to the Archive Version Object
                $null = Add-Member -InputObject $Version_ArchiveItemObject -MemberType NoteProperty -Name 'App_ArchiveItem' -Value $App_ArchiveItem
                $Version_ArchiveItemObject                

                if ($App_ArchiveItem.Name -eq $stringVersion_Current) {
                    # Set the Current Version variables with newly parsed values
                    Write-Host "Changing current version variables to newly parsed variables." -ForegroundColor Yellow
                    $versionVersion_CurrentObject = $Version_ArchiveItemObject
                    $versionVersion_Current = $Version_ArchiveItemObject.Version
                    $versionVersion_Latest = $versionVersion_Current
                }
            }
        }
        ## END parsing Version Items in Archive Directory

        ## START Latest Version comparison
        foreach ($Version_ArchiveItemObject in ($Version_ArchiveItemObjectList | Where-Object {$_.Version})) {
            Write-Host "Checking if $($Version_ArchiveItemObject.Version) ($($Version_ArchiveItemObject.String)) is greater than $versionVersion_Latest ($stringVersion_Latest)" -ForegroundColor Yellow
            if ($Version_ArchiveItemObject.Version -gt $versionVersion_Latest) {

                Write-Host "$($Version_ArchiveItemObject.String)" -ForegroundColor Green -NoNewline
                Write-Host " is a newer version than " -NoNewline
                Write-Host "$stringVersion_Latest" -ForegroundColor Red
            
                $ArchitectureDirectoryList = Get-ChildItem -Path $Version_ArchiveItemObject.App_ArchiveItem.FullName -Directory
                if(($ArchitectureDirectoryList.FullName -like '*win_x86*') -or ($ArchitectureDirectoryList.FullName -like '*win_x64*')) {
                    $versionVersion_Latest = $Version_ArchiveItemObject.Version
                    $stringVersion_Latest = $Version_ArchiveItemObject.String
                    $App_VersionPathLatest = $Version_ArchiveItemObject.App_ArchiveItem
                } else {
                    Write-Host "   ... but the directory '$($Version_ArchiveItemObject.App_ArchiveItem.FullName)' does not contain a 'win_x86' nor a 'win_x64' directory. Discarding item." -ForegroundColor Yellow
                }
            }
        }
        ## END Latest Version comparison

        ## START building final output        
        # Name formatting for Java
        if ($App_VersionPath -like '*\Oracle\JRE\*') {
            $AppName = 'Java'
        } elseif ($App_VersionPath -like '*\Oracle\JDK\*') {
            $AppName = 'Java SE Development Kit'
        } else {
            if ($CMApp) {
                $AppName = $App_DeserializedXML.DisplayInfo[0].Title
                $AppFullName = $CMApp.LocalizedDisplayName

            } else {
                $AppName = $appBaseName
                $AppFullName = $appBaseName
            }
        }

        if ($versionVersion_Latest -gt $versionVersion_Current) {
            Write-Host "The latest version is " -NoNewline
            Write-Host "$stringVersion_Latest located in $($App_VersionPathLatest.FullName)" -ForegroundColor Green

            $objApp = [pscustomobject]@{
                NeedsUpdate    = $true
                AppName        = $AppName
                AppFullName    = $AppFullName
                AppPath        = $App_VersionPath
                CurrentVersion = $stringVersion_Current
                NewestVersion  = $stringVersion_Latest
            }

            $null = $Apps_ToBeUpdated.Add($objApp)
        } else {
            Write-Host "$stringVersion_Latest located in $App_VersionPath " -ForegroundColor Yellow -NoNewline
            Write-Host "is already the latest version."

            $objApp = [pscustomobject]@{
                NeedsUpdate    = $false
                AppName        = $AppName
                AppFullName    = $AppFullName
                AppPath        = $App_VersionPath
                CurrentVersion = $stringVersion_Current
                NewestVersion  = $stringVersion_Latest
            }

            $null = $Apps_ToBeUpdated.Add($objApp)
        }
        ## END building final output

        $App_ArchiveItemsLatest = $App_ArchiveItemList | Sort-Object -Property LastWriteTime -Descending | Select-Object -First 1
    }

    Write-Host "END $CmdletName" -ForegroundColor Green

    $Apps_ToBeUpdated
}
#endregion