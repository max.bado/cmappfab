﻿# Retire or remove one or more applications
[CmdletBinding()]
param(
    [parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [string[]]$RetiringAppNames,        # 'Mplus and Multilevel 7.3.1', 'Zoom 123',

    [parameter(Mandatory = $false)]
    [string]$RetireFolderName = 'Retired',

    [parameter(Mandatory = $false)]
    [bool]$Remove = $false,

    [parameter(Mandatory = $false)]
    [switch]$Wildcard,

    [parameter(Mandatory=$false)]
    [string]$CMServer,

    [parameter(Mandatory=$false)]
    [string]$CMSite
)

try {
    . "$PSScriptRoot\Load-ConfigMgrAssemblies.ps1"
    Load-ConfigMgrAssemblies -LoadModule
} catch {
    Write-Host "Unable to load ConfigMgr assemblies"
}


if (!$CMServer -or !$CMSite) {
    Write-Host "Either the CMServer or CMSite was not provided. Attempting to retrieve CMServer and CMSite from the ConfigMgr client"
    try {
        $SMSClientObject = New-Object -ComObject Microsoft.SMS.Client -Strict
        $CMServer = $SMSClientObject.GetCurrentManagementPoint()
        $CMSite = $SMSClientObject.GetAssignedSite()
    } catch {
        Write-Host "Unable to retrieve site server information from the ConfigMgr client. Please rerun the script with the site server information supplied"
    }
}

# Get list of CM Applications
Write-Host "Retrieving CM Application List."
$CMAppListQuery = "SELECT * FROM SMS_Application
    WHERE SMS_Application.IsLatest = 'True' 
    AND SMS_Application.IsEnabled = 'True' 
    AND SMS_Application.IsExpired = 'False'"

$CMAppList = $null
$CMAppList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query $CMAppListQuery


foreach($retiringAppName in $RetiringAppNames) {

    if(($retiringAppName -match '\*') -and ($Wildcard)) {
        Write-Host 'Name contains a wildcard and Wildcard switch was specified'
        $CMTargetAppList = $CMAppList | Where-Object {$_.LocalizedDisplayName -like "$retiringAppName"} | Select-Object -ExpandProperty LocalizedDisplayName
        foreach($CMTargetApp in $CMTargetAppList) {
            Write-Host "Working on $CMTargetApp"
            $retiringApp = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$CMTargetApp`""
            $oldDeployments = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationAssignment WHERE SMS_ApplicationAssignment.ApplicationName = `"$($retiringApp.LocalizedDisplayName)`""
            foreach($oldDeployment in $oldDeployments) {
                Write-Host "Removing Deployment: $($oldDeployment.AssignmentName)"
                Remove-CimInstance -InputObject $oldDeployment -Verbose
            }
            
            if($Remove) {
                Write-Host "Remove was specified. Deleting $CMTargetApp"
                $retiringApp = Get-CMApplication -Name $CMTargetApp
                Remove-CMApplication -InputObject $retiringApp -Force

            } else {
                Write-Host "Retiring $CMTargetApp"
                $appInstance = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$CMTargetApp`""
                Invoke-CimMethod -InputObject $appInstance -MethodName 'SetIsExpired'
                Write-Host "Moving $CMTargetApp to Retired folder"
                $retiringApp = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$CMTargetApp`""

                $APPScopeID = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$CMTargetApp`""
                if ($RetireFolderName) {
                    $TargetFolderID = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Class SMS_ObjectContainerNode -Filter "Name='$RetireFolderName' and ObjectType='6000' AND ParentContainerNodeID='0'"
                    $CurrentFolderID = 0
                    $ObjectTypeID = 6000
                    
                    $WMIConnection = [WMIClass]"\\$CMServer\root\SMS\Site_$($CMSite):SMS_objectContainerItem"
                    $MoveItem = $WMIConnection.psbase.GetMethodParameters("MoveMembers")
                    $MoveItem.ContainerNodeID = $CurrentFolderID
                    $MoveItem.InstanceKeys = $APPScopeID.ModelName
                    $MoveItem.ObjectType = $ObjectTypeID
                    $MoveItem.TargetContainerNodeID = $TargetFolderID.ContainerNodeID
                    $WMIConnection.psbase.InvokeMethod("MoveMembers",$MoveItem,$null)
                }

            }
        }
    } else {
        Write-Host "Working on $retiringAppName"
        $retiringApp = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$retiringAppName`""
        $oldDeployments = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationAssignment WHERE SMS_ApplicationAssignment.ApplicationName = `"$($retiringApp.LocalizedDisplayName)`""
        
        # Remove old deployments
        foreach($oldDeployment in $oldDeployments) {
            Write-Host "Removing Deployment: $($oldDeployment.AssignmentName)"
            Remove-CimInstance -InputObject $oldDeployment -Verbose
        }
        
        if($Remove) {
            Push-Location -Path "$($CMSite):"
            Write-Host "Remove was specified. Deleting $retiringAppName"
            $retiringApp = Get-CMApplication -Name $retiringAppName
            Remove-CMApplication -InputObject $retiringApp -Force
            Pop-Location
        } else {
            $appInstance = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$retiringAppName`""
            Invoke-CimMethod -InputObject $appInstance -MethodName 'SetIsExpired'
            $retiringApp = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$retiringAppName`""
            Write-Host "Moving $retiringAppName to Retired folder"

            $APPScopeID = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE SMS_ApplicationLatest.LocalizedDisplayName = `"$retiringAppName`""
            if ($RetireFolderName) {
                $TargetFolderID = Get-CimInstance -ComputerName $CMServer -Namespace "Root\SMS\Site_$CMSite" -Class SMS_ObjectContainerNode -Filter "Name='$RetireFolderName' and ObjectType='6000' AND ParentContainerNodeID='0'"
                $CurrentFolderID = 0
                $ObjectTypeID = 6000
                
                $WMIConnection = [WMIClass]"\\$CMServer\root\SMS\Site_$($CMSite):SMS_objectContainerItem"
                $MoveItem = $WMIConnection.psbase.GetMethodParameters("MoveMembers")
                $MoveItem.ContainerNodeID = $CurrentFolderID
                $MoveItem.InstanceKeys = $APPScopeID.ModelName
                $MoveItem.ObjectType = $ObjectTypeID
                $MoveItem.TargetContainerNodeID = $TargetFolderID.ContainerNodeID
                $WMIConnection.psbase.InvokeMethod("MoveMembers",$MoveItem,$null)
            }
        }
    }
}
