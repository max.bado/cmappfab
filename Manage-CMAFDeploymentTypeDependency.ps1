function Add-DeploymentTypeDependencyGroup {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $DependencyGroup,
        $AppCurrentDT,
        $CurrentDeploymentTypeDependencyGroupList,
        $CMAppList
    )
    # Add Dependency to the current Deployment Type     
    if ($DependencyGroup.DependencyGroupName -eq 'TempGroupName') {
        $DependencyGroup.DependencyGroupName = $DependencyGroup.DependencyApps.DependencyName -join ' OR '
    }

    $DependencyGroupName = $DependencyGroup.DependencyGroupName
    Write-Host "Gathering info on $DependencyGroupName." -ForegroundColor Cyan

    $DeploymentType = $DependencyGroup.DeploymentType

    $DependencyGroupExists = $false

    # Check if DependencyGroup already exists
    if ($CurrentDeploymentTypeDependencyGroupList) {
        Write-Host "Dependency Groups exist..."
        foreach ($CurrentDeploymentTypeDependencyGroup in $CurrentDeploymentTypeDependencyGroupList) {
            $CurrentDeploymentTypeDependencyGroupName = $null
            $CurrentDeploymentTypeDependencyGroupTargetOperands = $null

            $CurrentDeploymentTypeDependencyGroupName = $CurrentDeploymentTypeDependencyGroup.GroupName
            $CurrentDeploymentTypeDependencyGroupTargetOperands = $CurrentDeploymentTypeDependencyGroup.Rule.Expression.Operands

            Write-Host "Checking if existing Dependency Group '$CurrentDeploymentTypeDependencyGroupName' matches $DependencyGroupName."

            if ($CurrentDeploymentTypeDependencyGroupName -eq $DependencyGroupName) {
                Write-Host "A Dependency Group with the name '$CurrentDeploymentTypeDependencyGroupName' already exists. Setting DependencyGroupExists flag to True." -ForegroundColor Magenta

                $DependencyGroupExists = $true
                break
            }
        }

        Write-Host "Setting DependencyGroupExists property for $DependencyGroupName to $DependencyGroupExists."
        # Set Dependency Group's DependencyGroupExists property so that we can decide what to do with it later.
        $DependencyGroup.DependencyGroupExists = $DependencyGroupExists
    }

    # If the Dependency Group exists, check to see if the Dependency Apps exist also
    if ($DependencyGroup.DependencyGroupExists) {
        $CurrentDeploymentTypeDependencyGroup = $null
        $CurrentDeploymentTypeDependencyGroupName = $null
        $CurrentDeploymentTypeDependencyGroupTargetOperands = $null

        $CurrentDeploymentTypeDependencyGroup = $CurrentDeploymentTypeDependencyGroupList | Where-Object {$_.GroupName -eq $DependencyGroupName}
        $CurrentDeploymentTypeDependencyGroupName = $CurrentDeploymentTypeDependencyGroup.GroupName
        $CurrentDeploymentTypeDependencyGroupTargetOperands = $CurrentDeploymentTypeDependencyGroup.Rule.Expression.Operands

        Write-Host "Dependency Group exists, so we need to check if the Dependency Apps exist also."
        # Iterate through each Dependency App in the group
        foreach ($DependencyItem in $DependencyGroup.DependencyApps) {

            $DependencyName = $DependencyItem.DependencyName
            $DependencyArchitecture = $DependencyItem.DependencyArchitecture
            $DependencyApp = $DependencyItem.DependencyApp
            $DependencyAppDeploymentType = $DependencyItem.DependencyAppDeploymentType

            Write-Host "Checking if Dependency Item $DependencyName already exists in $DependencyGroupName." -ForegroundColor Yellow

            if (!$DependencyAppDeploymentType) {
                $DependencyAppDeploymentType = Get-CMDeploymentType -ApplicationName $DependencyApp.LocalizedDisplayName | Where-Object {$_.LocalizedDisplayName -like "*$DependencyArchitecture*"}
            }

            $DependencyAppExists = $false
            $DependencyAppSDMPackage = $DependencyItem.DependencyAppSDMPackage

            # Check if Dependency App exists within this Dependency Group
            foreach ($Operand in $CurrentDeploymentTypeDependencyGroupTargetOperands) {
                Write-Host "Working on Operand '$($Operand.ApplicationAuthoringScopeId)/$($Operand.ApplicationLogicalName)'" -ForegroundColor DarkYellow
                $CurrentDeploymentTypeDependencyGroupTargetApplication = $null
                $CurrentDeploymentTypeDependencyGroupTargetApplicationWMI = $null
                $CurrentDeploymentTypeDependencyGroupTargetApplicationSDM = $null
                $CurrentDeploymentTypeDependencyGroupTargetApplicationDeploymentType = $null

                $CurrentDeploymentTypeDependencyGroupTargetApplication = $CMAppList | Where-Object {
                    $_.ModelName -eq "$($Operand.ApplicationAuthoringScopeId)/$($Operand.ApplicationLogicalName)"
                }
                #$CurrentDeploymentTypeDependencyGroupTargetApplicationWMI = [wmi]$CurrentDeploymentTypeDependencyGroupTargetApplication.__PATH
                $CurrentDeploymentTypeDependencyGroupTargetApplicationWMI = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($CurrentDeploymentTypeDependencyGroupTargetApplication.CI_ID)"
                $CurrentDeploymentTypeDependencyGroupTargetApplicationSDM = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($CurrentDeploymentTypeDependencyGroupTargetApplicationWMI.SDMPackageXML)

                $CurrentDeploymentTypeDependencyGroupTargetApplicationDeploymentType = $CurrentDeploymentTypeDependencyGroupTargetApplicationSDM.DeploymentTypes | Where-Object {
                    $_.Name -eq $Operand.DeploymentTypeLogicalName
                }

                Write-Host "CurrentDeploymentTypeDependencyGroupTargetApplication = $($CurrentDeploymentTypeDependencyGroupTargetApplication.LocalizedDisplayName)" -ForegroundColor Gray
                Write-Host "CurrentDeploymentTypeDependencyGroupTargetApplicationDeploymentType = $($CurrentDeploymentTypeDependencyGroupTargetApplicationDeploymentType.Title)" -ForegroundColor Gray

                if ($Operand.DeploymentTypeLogicalName -in $DependencyAppSDMPackage.DeploymentTypes.Name) {
                    Write-Host "'$($DependencyItem.DependencyApp.LocalizedDisplayName) | $($CurrentDeploymentTypeDependencyGroupTargetApplicationDeploymentType.Title)' dependency already exists."
                    Write-Host "Setting DependencyExists flag."

                    $DependencyAppExists = $true
                    break
                }
            } # foreach Operand

            $DependencyItem.DependencyAppExists = $DependencyAppExists
        } # END foreach Dependency Item 
    } # END if Dependency Group exists

    if ($DependencyGroup.DependencyGroupExists) {
        # if group exists
        # don't remove app that exists
        Write-Host "Dependency Group with the name $DependencyGroupName already exists."
        $DependenciesThatExist = $DependencyGroup.DependencyApps | Where-Object {$_.DependencyAppExists}
        foreach ($DependencyThatExists in $DependenciesThatExist) {
            Write-Host "$($DependencyThatExists.DependencyName) already exists. Skipping." -ForegroundColor DarkRed
        }
        # remove all other apps
        $DependenciesToRemove = Get-CMDeploymentTypeDependency -InputObject $CurrentDeploymentTypeDependencyGroup | Where-Object {$_.AppModelName -notin $DependenciesThatExist.DependencyAppDeploymentType.AppModelName}
        foreach ($DependencyToRemove in $DependenciesToRemove) {
            Write-Host "Removing Dependency App $($DependencyToRemove.LocalizedDisplayName) in Group $($CurrentDeploymentTypeDependencyGroup.GroupName) in DeploymentType $($appCurrentDT.LocalizedDisplayName)." -BackgroundColor Red
            Remove-CMDeploymentTypeDependency -DeploymentTypeDependency $DependencyToRemove -InputObject $CurrentDeploymentTypeDependencyGroup -Force
        }
        # create apps that don't exist
        $DependenciesToCreate = $DependencyGroup.DependencyApps | Where-Object {!($_.DependencyAppExists)}
        foreach ($DependencyToCreate in $DependenciesToCreate) {
            $DependencyName = $DependencyToCreate.DependencyName
            $DependencyArchitecture = $DependencyToCreate.DependencyArchitecture
            $DependencyApp = $DependencyToCreate.DependencyApp
            $DependencyAppDeploymentType = $DependencyToCreate.DependencyAppDeploymentType

            Write-Host "Working on creating Dependency Item $DependencyName in existing Group $DependencyGroupName."
            $NewDeploymentTypeDependency = $null
            $NewDeploymentTypeDependency = try {
                Add-CMDeploymentTypeDependency -IsAutoInstall $true -InputObject $CurrentDeploymentTypeDependencyGroup -DeploymentTypeDependency $DependencyAppDeploymentType
            } catch {}

            $counter = 1
            while (!$NewDeploymentTypeDependency) {
                Start-Sleep -Seconds 1
                Write-Host $counter
                #Write-Host "-------------NewCMDeploymentTypeDependencyGroup-------------"
                #Write-Host $NewCMDeploymentTypeDependencyGroup
                #Write-Host "-------------NewCMDeploymentType-------------"
                #Write-Host $NewCMDeploymentType

                #Write-Host (Get-CMObjectLockDetails -InputObject $NewCMDepl

                $NewDeploymentTypeDependency = try {
                    Add-CMDeploymentTypeDependency -IsAutoInstall $true -InputObject $CurrentDeploymentTypeDependencyGroup -DeploymentTypeDependency $DependencyAppDeploymentType
                } catch {}
            
                $counter++
            }
        }            
    } else {
        # if group doesn't exist
        Write-Host "Dependency Group $DependencyGroupName does not exist. Creating it."
        # create group
        $NewCMDeploymentTypeDependencyGroup = New-CMDeploymentTypeDependencyGroup -GroupName $DependencyGroupName -InputObject $DeploymentType
        # create apps
        foreach ($DependencyItem in $DependencyGroup.DependencyApps) {
            $DependencyName = $DependencyItem.DependencyName
            $DependencyArchitecture = $DependencyItem.DependencyArchitecture
            $DependencyApp = $DependencyItem.DependencyApp
            $DependencyAppDeploymentType = $DependencyItem.DependencyAppDeploymentType

            Write-Host "Working on creating Dependency Item $DependencyName in existing Group $DependencyGroupName."

            if (!$DependencyAppDeploymentType) {
                $DependencyAppDeploymentType = Get-CMDeploymentType -ApplicationName $DependencyApp.LocalizedDisplayName | Where-Object {$_.LocalizedDisplayName -like "*$DependencyArchitecture*"}
            } elseif ($DependencyApp.LocalizedDisplayName -eq $appName) {
                $DependencyAppDeploymentType = Get-CMDeploymentType -ApplicationName $DependencyApp.LocalizedDisplayName | Where-Object {$_.LocalizedDisplayName -like "*$DependencyArchitecture*"}
            }
            
            $NewDeploymentTypeDependency = $null
            $NewDeploymentTypeDependency = try {
                Add-CMDeploymentTypeDependency -IsAutoInstall $true -InputObject $NewCMDeploymentTypeDependencyGroup -DeploymentTypeDependency $DependencyAppDeploymentType
            } catch {}

            $counter = 1
            while (!$NewDeploymentTypeDependency) {
                Start-Sleep -Seconds 1
                Write-Host $counter

                $NewDeploymentTypeDependency = try {
                    Add-CMDeploymentTypeDependency -IsAutoInstall $true -InputObject $NewCMDeploymentTypeDependencyGroup -DeploymentTypeDependency $DependencyAppDeploymentType
                } catch {}
                
                $counter++
            }
        } # END foreach Dependency Item 
    } # END if Dependency Group Exists
}

function New-DeploymentTypeDependencyApplicationObject {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $Dependency,
        $appName,
        $CMAppList
    )
    #region -----------------------------------------------------------[Format Dependency Name]------------------------------------------------------------
    $appDependencyXMLPath = $null
    $appDependencyXMLContent = $null
    $appDependencyXML = $null
    $DependencyName = $null
    $appDependencyNameSuffix = $null
    $appDependencyBaseName = $null

    if ($Dependency.Name -eq '*SELF*') {
        $DependencyName = $appBaseName
        $appDependencyXMLPath = (Get-Item -Path "$XmlDirectory\$($DependencyName).xml").FullName
        $appDependencyXMLContent = (Get-Content -Path $appDependencyXMLPath)
        $appDependencyXML = [xml]$appDependencyXMLContent
    } else {
        $appDependencyXMLPath = (Get-Item -Path "$XmlDirectory\$($Dependency.Name).xml").FullName
        $appDependencyXMLContent = (Get-Content -Path $appDependencyXMLPath)
        $appDependencyXML = [xml]$appDependencyXMLContent
        # Make special consideration for Applications with a suffix such as 'for Faculty and Staff' or 'for Labs', etc.
        $appDependencyNameSuffix = $appDependencyXML.Application.DisplayInfo.Suffix

        if ($appDependencyNameSuffix) {
            $appDependencyBaseName = $Dependency.Name.Replace($appDependencyNameSuffix, $null)
            $appDependencyBaseName = $appDependencyBaseName.Remove($appDependencyBaseName.Length - 1)
            $DependencyName = $appDependencyBaseName
        } else {
            $appDependencyBaseName = $Dependency.Name
            $DependencyName = $appDependencyBaseName
        }
    }
    #endregion -----------------------------------------------------------[END Format Dependency Name]------------------------------------------------------------
    <#
    if ($UpdateDependencies) {
        if ($DependencyName -eq $appName) {
            Write-Host "Can't use the UpdateDependencies switch when the application has a dependency on a DeploymentType from itself." -ForegroundColor Red
        } else {
            $BuildAppForDependencyProps = @{
                appXMLPath      = "$XmlDirectory\$DependencyName.xml"
                NewVersion      = 'Latest'
                RetireOld       = $RetireOldDependencies.IsPresent
                CopyDeployments = $CopyDependenciesDeployments.IsPresent
            }

            Write-Host "Pop-Location: $(Pop-Location -PassThru)"
            #$null = .\Build-Application.ps1 @BuildAppForDependencyProps
        
            try {
                $null = .\Build-Application.ps1 @BuildAppForDependencyProps
            } catch {
                Write-Error $_.Exception
            }
        
            Write-Host "Push-Location: $(Push-Location -Path 'MU1:' -PassThru)"
        }
    } # END UpdateDependencies
    #>
    Write-Host "Running WMI query to set appDependencyApp for $DependencyName."
    $AppDependencyApp = $CMAppList | Where-Object {$_.LocalizedDisplayName -like "$DependencyName*"}
    <#
    $AppDependencyApp = (Get-WmiObject -ComputerName $CMServer -Namespace "root/sms/site_$CMSite" -Query "
            SELECT * FROM SMS_Application 
            WHERE LocalizedDisplayName LIKE `"$DependencyName%`" 
            AND SMS_Application.ISLatest='True' 
            AND SMS_Application.IsEnabled = 'True' 
            AND SMS_Applications.IsExpired = 'False'") | Sort-Object -Descending | Select-Object -First 1
    #>
    #$AppDependencyAppWMI = [wmi]$AppDependencyApp.__PATH
    $AppDependencyAppWMI = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($AppDependencyApp.CI_ID)"
    $AppDependencyAppSDM = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($AppDependencyAppWMI.SDMPackageXML)

    Write-Host "Running Get-CMDeploymentType to set appDependencyAppDT for $($AppDependencyApp.LocalizedDisplayName)."

    $AppDependencyAppDTList = Get-CMDeploymentType -ApplicationName $AppDependencyApp.LocalizedDisplayName

    if ($Dependency.DeploymentTypeArchitecture) {
        $appDependencyAppDT = $AppDependencyAppDTList | Where-Object {$_.LocalizedDisplayName -like "*$($Dependency.DeploymentTypeArchitecture)*"}
        $DependencyName = "$DependencyName $($Dependency.DeploymentTypeArchitecture)"
    } else {
        $appDependencyAppDT = $AppDependencyAppDTList | Select-Object -First 1
        #$DependencyName = $appBaseName
    }

    $AppDependencyAppObject = [pscustomobject]@{
        DependencyName              = $DependencyName
        DependencyArchitecture      = $Dependency.DeploymentTypeArchitecture
        DependencyApp               = $AppDependencyApp
        DependencyAppDeploymentType = $appDependencyAppDT
        DependencyAppExists         = $null
        DependencyAppSDMPackage     = $AppDependencyAppSDM
    }

    Write-Host "Setting appDependencyObject with
    DependencyName = $DependencyName
    DependencyApp = $($AppDependencyApp.LocalizedDisplayName)
    DependencyAppDeploymentType = $($appDependencyAppDT.LocalizedDisplayName)"

    $AppDependencyAppObject
}

function New-DeploymentTypeDependencyGroup {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $appDTDependency,
        $appName
    )

    $DependencyApplication = $appDTDependency.Application
    $DependencyApplicationName = $DependencyApplication.Name

    $DependencyApplicationArchitecture = $DependencyApplication.DeploymentTypeArchitecture

    if ($DependencyApplicationName -eq '*SELF*') {
        $DependencyApplicationName = $appName
    }

    # Format Dependency Group name
    if ($DependencyApplicationArchitecture) {
        $DependencyGroupName = "$DependencyApplicationName $DependencyApplicationArchitecture"
    } else {
        $DependencyGroupName = $DependencyApplicationName
    }

    # Create new DependencyGroup Object
    $DependencyGroupObject = [pscustomobject]@{
        GroupName   = $DependencyGroupName
        Application = $DependencyApplication
    }
        
    # Return the newly formed DependencyGroup Object
    $DependencyGroupObject
}   

function New-DeploymentTypeDependencyGroupObject {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $DependencyGroup,
        $AppCurrentDT,
        $CMAppList

    )

    if ($DependencyGroup.GroupName) {
        $DependencyGroupName = $DependencyGroup.GroupName
    } else {
        $DependencyGroupName = 'TempGroupName'
    }

    [System.Collections.Generic.List[pscustomobject]]$AppDependencyAppCol = @()
    
    $AppDependencyGroupObject = [pscustomobject]@{
        DependencyGroupName   = $DependencyGroupName
        DependencyApps        = $AppDependencyAppCol
        DeploymentType        = $AppCurrentDT
        DependencyGroupExists = $null
    }

    foreach ($Dependency in $DependencyGroup.Application) {
        $AppDependencyAppObject = New-DeploymentTypeDependencyApplicationObject -Dependency $Dependency -CMAppList $CMAppList
        $null = $AppDependencyGroupObject.DependencyApps.Add($AppDependencyAppObject)                
    } # END foreach Dependency

    $AppDependencyGroupObject

    #$null = $AppDTDependencyCol.Add($AppDependencyGroupObject)
}

function Manage-DeploymentTypeDependency {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $AppDT,
        $AppDeploymentTypes,
        $NewCMDeploymentType,
        $AppName,
        $CMAppList
    )

    $AppDTDependencyCol = $null
    $appDTTitle = $appDT.appDTTitle
    $appDTDependency = $appDT.appDTDependency

    if ($AppDeploymentTypes) {
        $AppCurrentDT = $AppDeploymentTypes | Where-Object {$_.LocalizedDisplayName -eq $appDTTitle}

        $CurrentDeploymentTypeDependencyGroupList = $null
        $CurrentDeploymentTypeDependencyGroupList = Get-CMDeploymentTypeDependencyGroup -InputObject $AppCurrentDT
    } else {
        $AppCurrentDT = $NewCMDeploymentType
    }
    Write-Host "Working on Deployment Type $appDTTitle."
    # Get Dependency information
    if ($appDTDependency) {
        Write-Host  "Deployment Type '$appDTTitle' has dependencies. We're going to iterate through the deployment types and make sure they have all the dependencies that they're supposed to have."

        $DependencyGroupList = $appDTDependency.Group
        # Create new Dependency Group object if the XML isn't using Dependency Groups to nest the Dependency Applications.
        if (!$DependencyGroupList) {
            $DependencyGroupList = foreach ($DependencyApplication in $appDTDependency.Application) {
                New-DeploymentTypeDependencyGroup -appDTDependency $DependencyApplication -appName $AppName
            } # END foreach $DependencyApplication in $appDTDependency.Application
        } # END if XML missing Dependency Groups

        $AppDTDependencyCol = foreach ($DependencyGroup in $DependencyGroupList) {
            $AppDependencyGroupObject = New-DeploymentTypeDependencyGroupObject -DependencyGroup $DependencyGroup -AppCurrentDT $AppCurrentDT -CMAppList $CMAppList
            $AppDependencyGroupObject
            #$null = $AppDTDependencyCol.Add($AppDependencyGroupObject)
        } # END foreach Dependency Group
    } # END if Dependency exists

    Write-Host "AppDTDependencyCol count = $($AppDTDependencyCol.Count)"

    # Add Dependency to the current Deployment Type
    foreach ($DependencyGroup in ($AppDTDependencyCol | Where-Object {$_.DeploymentType -eq $AppCurrentDT})) {        
        Add-DeploymentTypeDependencyGroup -DependencyGroup $DependencyGroup -AppCurrentDT $AppCurrentDT -CurrentDeploymentTypeDependencyGroupList $CurrentDeploymentTypeDependencyGroupList        
    } # END foreach Dependency Group
    
    if ($AppDeploymentTypes) {
        # Check if the Deployment Type has Dependency Groups that shouldn't be there and remove them.
        $DependencyGroupsToBeRemoved = $CurrentDeploymentTypeDependencyGroupList | Where-Object {$_.GroupName -notin ($AppDTDependencyCol | Where-Object {$_.DeploymentType -eq $AppCurrentDT}).DependencyGroupName}
        foreach ($DependencyGroupToBeRemoved in $DependencyGroupsToBeRemoved) {
            Write-Host "$($DependencyGroupToBeRemoved.GroupName) should not exist. Removing." -ForegroundColor Red
            Remove-CMDeploymentTypeDependencyGroup -InputObject $DependencyGroupToBeRemoved -Force
        }
    }
}