function New-CMAFDeploymentTypeObject {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $AppDT,
        $appFriendlyName, ##
        [Parameter(Mandatory = $true)]
        [string]$AppSoftwareVersion
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $appDTArchitecture = $appDT.Architecture
    $appDTTechnology = $appDT.Technology
    $appDTDetectionMethod = $appDT.DetectionMethod
    $appDTContentLocation = $appDT.ContentLocation
    $appDTInstallCommand = $appDT.InstallCommand
    $appDTUninstallCommand = $appDT.UninstallCommand
    $appDTRepairCommand = $appDT.RepairCommand
    $appDTScriptRunAs32Bit = $appDT.DetectionScript.RunAs32Bit
    $appDTScriptLanguage = $appDT.DetectionScript.Language
    $appDTScriptText = $appDT.DetectionScript.TEXT
    $appDTDependency = $appDT.Dependencies
    $appDTRequirements = $appDT.Requirements
    $AppDTSupersedes = $AppDT.Supersedes
    $AppDTExecutionContext = $AppDT.ExecutionContext
    $appDTRegPath = $null
    $appDTPSAppDeploy = $null
    $appDTTitle = $null
    $DetectionStatement = $null

    # Format Application Deployment Type Name
    # Append PSAppDeploy to the Deployment Type name if the installer uses the PSADT framework.
    if ($appDTInstallCommand -like '*Deploy-Application.exe*') {
        $appDTPSAppDeploy = 'PSAppDeploy'
    } else {
        $appDTPSAppDeploy = ''
    }
    $appDTTitle = "$appFriendlyName $AppSoftwareVersion $appDTArchitecture $appDTPSAppDeploy"

    # Format Application Deployment Type Content Location
    $appDTContentLocation = $appDTContentLocation -replace ('{SoftwareVersion}', $AppSoftwareVersion)
    $appDTContentLocation = $appDTContentLocation -replace ('{Architecture}', $appDTArchitecture)
    
    # Validate Content Location
    if(!(Test-Path -Path $appDTContentLocation)) {
        Write-Host "Path '$appDTContentLocation' does not exist. Stop creating Application Deployment Type '$appDTTitle'" -ForegroundColor Red
        return
    }

    # Format Application Deployment Type Install/Uninstall Commands
    $appDTInstallCommand = $appDTInstallCommand -replace ('{SoftwareVersion}', $AppSoftwareVersion)
    $appDTUninstallCommand = $appDTUninstallCommand -replace ('{SoftwareVersion}', $AppSoftwareVersion)
    $appDTRepairCommand = $appDTRepairCommand -replace ('{SoftwareVersion}', $AppSoftwareVersion)

    # Set Detection RegKey Path based on Deployment Type architecture
    switch ($appDTArchitecture) {
        'x86' {$appDTRegPath = 'HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall'}
        'x64' {$appDTRegPath = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'}
    }

    # Custom DetectionVersion Logic
    if ($appDT.Custom.DetectionVersionLogic) {
        $PropertyList = @{
            AppDT              = $AppDT
            AppSoftwareVersion = $AppSoftwareVersion
        }
        $DetectionVersion = Parse-CustomDetectionVersionLogic @PropertyList | Select-Object -First 1
    } else {
        $DetectionVersion = $AppSoftwareVersion
    }

    # Custom DetectionScript Logic
    if ($appDT.Custom.DetectionScriptLogic) {
        $PropertyList = @{
            AppDT              = $AppDT
            AppSoftwareVersion = $AppSoftwareVersion
            AppDTRegPath       = $AppDTRegPath
            DetectionVersion   = $DetectionVersion
        }
        $DetectionStatement = Parse-CustomDetectionScriptLogic @PropertyList
    }

    # Custom DetectionName Logic
    if ($appDT.Custom.DetectionNameLogic) {
        $PropertyList = @{
            AppDT              = $AppDT
            AppSoftwareVersion = $AppSoftwareVersion
        }
        $DetectionName = Parse-CustomDetectionNameLogic @PropertyList
    }

    # Format Application Deployment Type Script Text
    if ($DetectionStatement) {
        $appDTScriptText = $appDTScriptText.Replace('{DetectionStatement}', $DetectionStatement)
        # $appDTScriptText | Out-File -FilePath "$PSScriptRoot\ScriptText_$($appDTTitle).txt" -Force # debugging purposes
    } else {
        $appDTScriptText = $appDTScriptText.Replace('{SoftwareVersion}', $AppSoftwareVersion)
        $appDTScriptText = $appDTScriptText.Replace('{DetectionVersion}', $DetectionVersion)
        $appDTScriptText = $appDTScriptText.Replace('{Architecture}', $appDTArchitecture) 
        $appDTScriptText = $appDTScriptText.Replace('{DetectionName}', $DetectionName)
        $appDTScriptText = $appDTScriptText.Replace('{DetectionRegPath}', $appDTRegPath)
    }

    $appDTScriptText = $appDTScriptText.Replace('{DetectionStatement}', $DetectionStatement)
    $appDTScriptText = $appDTScriptText.Replace('{SoftwareVersion}', $AppSoftwareVersion)
    $appDTScriptText = $appDTScriptText.Replace('{DetectionVersion}', $DetectionVersion)
    $appDTScriptText = $appDTScriptText.Replace('{Architecture}', $appDTArchitecture) 
    $appDTScriptText = $appDTScriptText.Replace('{DetectionName}', $DetectionName)
    $appDTScriptText = $appDTScriptText.Replace('{DetectionRegPath}', $appDTRegPath)
    
    # Default ExecutionContext is System
    if (!$AppDTExecutionContext) {
        $AppDTExecutionContext = 'System'
    }

    if($ScriptOutputTest) {
        $appDTScriptText | Out-File -FilePath "$PSScriptRoot\ScriptText\ScriptText_$($appDTTitle).txt" -Force # debugging purposes
    }

    # Create Application Deployment Type Object
    $appDTObj = [pscustomobject]@{
        appDTTitle            = $appDTTitle
        appDTArchitecture     = $appDTArchitecture
        appDTTechnology       = $appDTTechnology
        appDTDetectionMethod  = $appDTDetectionMethod
        appDTContentLocation  = $appDTContentLocation
        appDTInstallCommand   = $appDTInstallCommand
        appDTUninstallCommand = $appDTUninstallCommand
        appDTRepairCommand    = $appDTRepairCommand
        appDTScriptRunAs32Bit = $appDTScriptRunAs32Bit
        appDTScriptLanguage   = $appDTScriptLanguage
        appDTScriptText       = $appDTScriptText
        AppDTExecutionContext = $AppDTExecutionContext
        appDTDependency       = $appDTDependency        
        appDTRequirements     = $appDTRequirements
        AppDTSupersedes       = $AppDTSupersedes
    }

    Write-Host "END $CmdletName" -ForegroundColor Green

    $appDTObj
}