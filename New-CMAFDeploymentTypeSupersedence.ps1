﻿function New-CMAFDeploymentTypeSupersedenceObject {
    [CmdletBinding()]
    Param(
        $AppDT
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $DTSupersedenceList = $AppDT.AppDTSupersedes

    $SupersedenceObjectList = [System.Collections.Generic.List[pscustomobject]]::new()

    $AppAuthoringScopeId = 'ScopeId_D35536D7-3D95-4325-A433-BBF593813197'
    $DtAuthoringScopeId = 'ScopeId_D35536D7-3D95-4325-A433-BBF593813197'
    $AppVersion = 0 # Latest App version
    $DtVersion = 0 # Latest DT version
    $DesiredState = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeDesiredState]::Prohibited
    #$DependencyApplicationOperator = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ExpressionOperators.ExpressionOperator]::Or # NOT needed
    $NoncomplianceSeverity = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.NoncomplianceSeverity]::None

    # Uninstall = $true; Don't Uninstall = $false
    $EnforceDesiredState = $true 

    foreach($SupersededApplication in $DTSupersedenceList.Application) {
        
        ##### This is NOT needed unlike when composing Dependency Rules #####
        # Collection of Superseded Deployment Types that will be added to the Superseded Application Rule        
        #$IntentExpressionCollection = $null
        #$IntentExpressionCollection = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeIntentExpression]]::new()
        #####################################################################

        # Get the Application info
        $CMSupersededApplication = $null
        $CMSupersededApplication = $CMAppList | Where-Object {$_.LocalizedDisplayName -match "$($SupersededApplication.Name)"}
        $CMSupersededApplicationWMI = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($CMSupersededApplication.CI_ID)"
        $CMSupersededApplicationSDM = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($CMSupersededApplicationWMI.SDMPackageXML)

        foreach($SupersededApplicationDeploymentType in $SupersededApplication.DeploymentType) {

            # Get the Deployment Type info
            if($SupersededApplicationDeploymentType.Name) {
                # Match the Deployment Type if the name is specified
                $TargetDeploymentType = $CMSupersededApplicationSDM.DeploymentTypes | Where-Object {$_.Title -match $SupersededApplicationDeploymentType.Name}
            } elseif ($SupersededApplicationDeploymentType.DeploymentTypeArchitecture) {
                # Match the Deployment Type if the architecture is specified
                $TargetDeploymentType = $CMSupersededApplicationSDM.DeploymentTypes | Where-Object {$_.Title -match $SupersededApplicationDeploymentType.DeploymentTypeArchitecture}                
            } else {
                $TargetDeploymentType = $CMDependencyApplicationSDM.DeploymentTypes[0]
            }

            # Compose the Supersedence object
            $AppLogicalName = $CMSupersededApplicationSDM.Name # Define the target application
            $DtLogicalName = $TargetDeploymentType.Name # Define the target application's target dependency type

            # Create the Intent Expression
            $DeploymentTypeIntentExpression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeIntentExpression]::new($AppAuthoringScopeId, $AppLogicalName, $AppVersion, $DtAuthoringScopeId, $DtLogicalName, $DtVersion, $DesiredState, $EnforceDesiredState)

            # Each Superseded Deployment Type gets its own Rule
            $RuleId = "DTRule_$([guid]::NewGuid())"
            $NewDeploymentTypeRule = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.DeploymentTypeRule]::new($RuleId, $NoncomplianceSeverity, $DeploymentTypeIntentExpression)   
            $null = $SupersedenceObjectList.Add($NewDeploymentTypeRule)   
        }
    }

    Write-Host "END $CmdletName" -ForegroundColor Green

    $SupersedenceObjectList
}