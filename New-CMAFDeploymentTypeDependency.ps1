﻿function New-CMAFDeploymentTypeDependencyObject {
    [CmdletBinding()]
    Param(
        $AppDT
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $DTDependencyList = $AppDT.AppDTDependency

    $DependencyObjectList = [System.Collections.Generic.List[pscustomobject]]::new()

    $DependencyXMLPath = $null
    $DependencyXMLContent = $null
    $DependencyXML = $null
    $DependencyName = $null
    $DependencyNameSuffix = $null
    $DependencyBaseName = $null


    $AppAuthoringScopeId = 'ScopeId_D35536D7-3D95-4325-A433-BBF593813197'
    $DtAuthoringScopeId = 'ScopeId_D35536D7-3D95-4325-A433-BBF593813197'
    $AppRevision = 0 # Latest App version
    $DtVersion = 0 # Latest DT version
    $DesiredState = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeDesiredState]::Required
    $DependencyApplicationOperator = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ExpressionOperators.ExpressionOperator]::Or
    $NoncomplianceSeverity = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.NoncomplianceSeverity]::Critical

    $EnforceDesiredState = $true


    foreach($DependencyGroup in $DTDependencyList.Group) {
        
        # Collection of Dependencies that will be added to the Dependency Group
        $IntentExpressionCollection = $null
        $IntentExpressionCollection = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeIntentExpression]]::new()

        foreach($DependencyApplication in $DependencyGroup.Application) {
            $AppVersion = $null
            if($DependencyApplication.Version) {
                $AppVersion = $DependencyApplication.Version
            } else {
                $AppVersion = 'Latest'
            }

            if ($DependencyApplication.Name -eq '*SELF*') {

                <#
                $DependencyName = $appBaseName
                $appDependencyXMLPath = (Get-Item -Path "$XmlDirectory\$($DependencyName).xml").FullName
                $appDependencyXMLContent = (Get-Content -Path $appDependencyXMLPath)
                $appDependencyXML = [xml]$appDependencyXMLContent

                # Get the Application info
                $CMDependencyApplication = $null
                $CMDependencyApplication = $CMAppList | Where-Object {$_.LocalizedDisplayName -match "$($DependencyName)"}
                $CMDependencyApplicationWMI = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($CMDependencyApplication.CI_ID)"
                $CMDependencyApplicationSDM = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($CMDependencyApplicationWMI.SDMPackageXML)

                # Get the Deployment Type info
                if($DependencyApplication.DeploymentTypeArchitecture) {
                    # Match the Deployment Type if the architecture is specified
                    $TargetDeploymentType = $CMDependencyApplicationSDM.DeploymentTypes | Where-Object {$_.Title -match $DependencyApplication.DeploymentTypeArchitecture}
                } else {
                    # If architecture is not specified, grab the first Deployment Type
                    $TargetDeploymentType = $CMDependencyApplicationSDM.DeploymentTypes[0]
                }

                # Compose the Dependency object
                $AppLogicalName = $CMDependencyApplicationSDM.Name # Define the target application
                $DtLogicalName = $TargetDeploymentType.Name # Define the target application's target dependency type

                # Create the Intent Expression and add it to the expression collection
                $DeploymentTypeIntentExpression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeIntentExpression]::new($AppAuthoringScopeId, $AppLogicalName, $AppRevision, $DtAuthoringScopeId, $DtLogicalName, $DtVersion, $DesiredState, $EnforceDesiredState)
                $null = $IntentExpressionCollection.Add($DeploymentTypeIntentExpression)
                #>
                
            } else {

                # Find the dependency application according to the version specified
                $CMDependencyApplicationList = $null
                $CMDependencyApplicationList = $CMAppList | Where-Object {$_.LocalizedDisplayName -like "*$($DependencyApplication.Name)*"}
                if($AppVersion -eq 'Latest') {
                    $CMDependencyApplication = $CMDependencyApplicationList | Sort-Object -Property SoftwareVersion -Descending | Select-Object -First 1
                } else {
                    $CMDependencyApplication = $CMDependencyApplicationList | Where-Object {$_.SoftwareVersion -eq $AppVersion}
                }

                # Get the dependency application info
                $CMDependencyApplicationWMI = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($CMDependencyApplication.CI_ID)"
                $CMDependencyApplicationSDM = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($CMDependencyApplicationWMI.SDMPackageXML)

                # Get the Deployment Type info
                if($DependencyApplication.DeploymentTypeArchitecture) {
                    # Match the Deployment Type if the architecture is specified
                    $TargetDeploymentType = $CMDependencyApplicationSDM.DeploymentTypes | Where-Object {$_.Title -match $DependencyApplication.DeploymentTypeArchitecture}
                } else {
                    # If architecture is not specified, grab the first Deployment Type
                    $TargetDeploymentType = $CMDependencyApplicationSDM.DeploymentTypes[0]
                }

                # Compose the Dependency object
                $AppLogicalName = $CMDependencyApplicationSDM.Name # Define the target application
                $DtLogicalName = $TargetDeploymentType.Name # Define the target application's target dependency type

                # Create the Intent Expression and add it to the expression collection
                $DeploymentTypeIntentExpression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeIntentExpression]::new($AppAuthoringScopeId, $AppLogicalName, $AppRevision, $DtAuthoringScopeId, $DtLogicalName, $DtVersion, $DesiredState, $EnforceDesiredState)
                $null = $IntentExpressionCollection.Add($DeploymentTypeIntentExpression)

            }
        }

        # Create the Dependency Application expression
        $DeploymentTypeExpression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeExpression]::new($DependencyApplicationOperator, $IntentExpressionCollection)

        # Define the Dependency Group / Rule
        $RuleId = "DTRule_$([guid]::NewGuid())"
        $Expression = $DeploymentTypeExpression
        $Annotation = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.Annotation]::new()

        # Format Dependency Group name
        if ($DependencyGroup.Name) {
            $DependencyGroupName = $DependencyGroup.Name
        } else {
            $DependencyGroupName = "$($DependencyGroup.Application.Name)" + "$($DependencyGroup.Application.DeploymentTypeArchitecture)" -join ' OR '
        }

        $DisplayName = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.LocalizableString]::new('DisplayName' ,$DependencyGroupName, $null)
        #$DisplayName = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.LocalizableString]::new('')
        #$Description = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.LocalizableString]::new($null ,'This is a test group', $null)

        $Annotation.DisplayName = $DisplayName
        #$Annotation.Description = $Description

        # Create the Dependency Group
        $NewDeploymentTypeRule = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.DeploymentTypeRule]::new($RuleId, $NoncomplianceSeverity, $Annotation, $Expression)   
        
        $null = $DependencyObjectList.Add($NewDeploymentTypeRule)   
    }

    <#
    if ($Dependency.Name -eq '*SELF*') {
        $DependencyName = $appBaseName
        $appDependencyXMLPath = (Get-Item -Path "$XmlDirectory\$($DependencyName).xml").FullName
        $appDependencyXMLContent = (Get-Content -Path $appDependencyXMLPath)
        $appDependencyXML = [xml]$appDependencyXMLContent
    } else {
        $appDependencyXMLPath = (Get-Item -Path "$XmlDirectory\$($Dependency.Name).xml").FullName
        $appDependencyXMLContent = (Get-Content -Path $appDependencyXMLPath)
        $appDependencyXML = [xml]$appDependencyXMLContent
        # Make special consideration for Applications with a suffix such as 'for Faculty and Staff' or 'for Labs', etc.
        $appDependencyNameSuffix = $appDependencyXML.Application.DisplayInfo.Suffix

        if ($appDependencyNameSuffix) {
            $appDependencyBaseName = $Dependency.Name.Replace($appDependencyNameSuffix, $null)
            $appDependencyBaseName = $appDependencyBaseName.Remove($appDependencyBaseName.Length - 1)
            $DependencyName = $appDependencyBaseName
        } else {
            $appDependencyBaseName = $Dependency.Name
            $DependencyName = $appDependencyBaseName
        }
    }

    #>

    Write-Host "END $CmdletName" -ForegroundColor Green

    $DependencyObjectList
}