#Requires -Version 4
<#
.SYNOPSIS
  This script creates new applications in Configuration Manager based off of the supplied application template.
.DESCRIPTION
  The script uses ConfigMgr DLLs, so the the system where the script is executed must have the Configuration Manager Console
  installed and the user executing the script must have ConfigMgr Site permissions to create and edit new applications.
  The user must also have permissions to query the Configuration Manager site server's WMI.
.PARAMETER AppTemplatePath
  Path to the Application Template XML. The path can be absolute or relative.
.PARAMETER NewVersion
  New version of the application to be created. The version must exist in the application source path defined in the Application XML Template.
.PARAMETER RetireOld
  When specified, any previous version of the application currently existing within ConfigMgr will be retired via the Retire-Application.ps1 script.
.PARAMETER DisableCurrentApplicationCheck
  When specified, the Get-CMAFApplicationUpdateVersions function is NOT executed. This prevents the usage of the following flags:
  - RetireOld
  - CopyDeployments
  - CopyCategories
.PARAMETER ScriptOutputTest
  When specified, the applcation is NOT built. Instead, the compiled detection script is output to a file in the .\ScriptText directory.
  The output file name has the following form: ScriptText_<Deployment Type Name>.txt
  One file is output for each Deployment Type.
.PARAMETER DisallowRepair
  When specified, the Allow Repair flag is disabled for each application deployment created by the script.
.PARAMETER CopyCategories
  When specified, the script attempts to copy the User and Software Categories from the previous version of the application that exists within ConfigMgr.
  Categories specified in the application build template are ignored.
.PARAMETER CopyDeployments
  When specified, the script attempts to copy the deployments from the previous version of the application that exists within ConfigMgr.
.PARAMETER RerunDiscovery
  When specified, the script will rerun the ConfigMgr environment discovery. If this switch is not specified, the ConfigMgr environment is cached
  within the session.
.PARAMETER CMServer
  The ConfigMgr server name. If this is not specified, the script attempts to discover the server name automatically.
.PARAMETER CMSite
  The ConfigMgr site name. If this is not specified, the script attempts to discover the site name automatically.
.PARAMETER LogFile
  Set a custom Log File Path. If this is not set, the default Log location is ".\Logs\Build-Application_<ApplicationTemplateName>.log".

.INPUTS
  Refer to Script Parameters.  
.OUTPUTS
  New Configuration Manager Application.
.NOTES
  Version:        1.0
  Author:         Max Bado
  Creation Date:  2/9/2018
  Purpose/Change: Initial script development
.EXAMPLE
  Create new application based off of the Google Chrome template. The latest available version within the application's source directory is used.

  .\Build-CMAFApplication.ps1 -AppTemplatePath '.Templates\XMLs\Google Chrome.xml' -NewVersion Latest
#>

[CmdletBinding()]
#[CmdletBinding(SupportsShouldProcess=$true)] # Use if you plan on accepting -whatif switch
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param(
    [Parameter(Mandatory = $true)]
    [ValidateScript(
        {
            if (Test-Path -Path $_ -ErrorAction SilentlyContinue) { 
                $true 
            } else {
                Throw "Please provide a valid App XML Template path."
            } 
        }
    )]
    [string[]]$AppTemplatePath,

    [Parameter(Mandatory = $true)]
    [ValidateNotNull()]
    [string]$NewVersion,
    
    [Parameter(Mandatory = $false)]
    [switch]$CopyDeployments,

    [Parameter(Mandatory = $false)]
    [switch]$RetireOld,

    [Parameter(Mandatory = $false)]
    [switch]$DisableCurrentApplicationCheck,

    [Parameter(Mandatory = $false)]
    [switch]$ScriptOutputTest, 

    [Parameter(Mandatory = $false)]
    [switch]$DisallowRepair,

    [Parameter(Mandatory = $false)]
    [switch]$CopyCategories,

    [Parameter(Mandatory = $false)]
    [switch]$RerunDiscovery,

    [parameter(Mandatory=$false)]
    [string]$CMServer,

    [parameter(Mandatory=$false)]
    [string]$CMSite,

    [Parameter(Mandatory = $false)]
    [Alias('LogPath')]
    [string]$LogFile
)
#endregion
#region ---------------------------------------------------------[Initialisations]--------------------------------------------------------

$VerbosePreference = 'SilentlyContinue'

try {
    . "$PSScriptRoot\Load-ConfigMgrAssemblies.ps1"
    Load-ConfigMgrAssemblies
} catch {
    Write-Host "Unable to load ConfigMgr assemblies"
    break
}

# Load dependencies
. $PSScriptRoot\Get-CMAFApplicationUpdatedVersions.ps1
. $PSScriptRoot\New-CMAFDeploymentTypeObject.ps1
. $PSScriptRoot\Parse-CMAFCustomDetection.ps1
. $PSScriptRoot\Manage-CMAFDeploymentTypeDependency.ps1
. $PSScriptRoot\New-CMAFDeploymentTypeRequirement.ps1
. $PSScriptRoot\New-CMAFDeploymentTypeDependency.ps1
. $PSScriptRoot\New-CMAFDeploymentTypeSupersedence.ps1
. $PSScriptRoot\New-CMAFApplication.ps1
. $PSScriptRoot\New-CMAFApplicationDeployment.ps1

# Create Logs directory


#endregion

#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName

#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region --------------------------------------------------[Event Log Write-Log Function]--------------------------------------------------

function Write-Log {
    
    <#
    .Synopsis
       Write-Log writes a message to a specified log file with the current time stamp.
    .DESCRIPTION
       The Write-Log function is designed to add logging capability to other scripts.
       In addition to writing output and/or verbose you can write to a log file for
       later debugging.
    .NOTES
       Created by: Jason Wasser @wasserja
       Modified: 11/24/2015 09:30:19 AM  
    
       Changelog:
        * Code simplification and clarification - thanks to @juneb_get_help
        * Added documentation.
        * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
        * Revised the Force switch to work as it should - thanks to @JeffHicks
    
       To Do:
        * Add error handling if trying to create a log file in a inaccessible location.
        * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
          duplicates.
    .PARAMETER Message
       Message is the content that you wish to add to the log file. 
    .PARAMETER Path
       The path to the log file to which you would like to write. By default the function will 
       create the path and file if it does not exist. 
    .PARAMETER Level
       Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
    .PARAMETER NoClobber
       Use NoClobber if you do not wish to overwrite an existing file.
    .EXAMPLE
       Write-Log -Message 'Log message' 
       Writes the message to c:\Logs\PowerShellLog.log.
    .EXAMPLE
       Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
       Writes the content to the specified log file and creates the path and file specified. 
    .EXAMPLE
       Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
       Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
    .LINK
       https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
    #>
    
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,
    
        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path = 'C:\Logs\PowerShellLog.log',
            
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
            
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )
    
    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
            
        Write-Verbose "Path: $Path"
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path -Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }
    
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path -Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item -Path $Path -Force -ItemType File
        }
    
        else {
            # Nothing to see here yet.
        }
    
        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
            
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion
#endregion
#region -----------------------------------------------------------[Execution]------------------------------------------------------------

## VARIABLE DECLARATIONS
 
$Global:AppTemplatePath = (Get-Item -Path $AppTemplatePath).FullName
$Global:XmlDirectory = (Get-Item -Path (Split-Path $AppTemplatePath -Parent)).FullName
$Global:AppTemplateName = (Get-Item -Path $AppTemplatePath).BaseName
$Global:AppReleaseDate = Get-Date

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    if ($AppTemplatePath.Count -gt 1) {
        $LogFile = "$PSScriptRoot\Logs\$($ScriptName)_MultipleApps_$(Get-Date -Format yyyy-mm-ddThh-mm).log"
    } else {
        $LogFile = "$PSScriptRoot\Logs\$($ScriptName)_$AppTemplateName.log"
    }    
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        if ($AppTemplatePath.Count -gt 1) {
            $LogFile = "$PSScriptRoot\Logs\$($ScriptName)_MultipleApps_$(Get-Date -Format yyyy-mm-ddThh-mm).log"
        } else {
            $LogFile = "$PSScriptRoot\Logs\$($ScriptName)_$AppTemplateName.log"
        }  
        Write-Log -Path $LogFile -Level Warning -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}

Write-Host "Log File located at: $LogFile"

# If CMServer or CMSite were not specified, retrieve the info from the Microsoft.SMS.Client object 
if (!($CMServer) -or !($CMSite)) {
    $Global:SMSClientObject = New-Object -ComObject Microsoft.SMS.Client -Strict
    if (!$CMServer) {
        $Global:CMServer = $SMSClientObject.GetCurrentManagementPoint()
    }
    if (!$CMSite) {
        $Global:CMSite = $SMSClientObject.GetAssignedSite()
    }
} else {
    $Global:CMServer = $CMServer
    $Global:CMSite = $CMSite
}

# Get list of CM Applications
if ((!$CMAppList) -or $RerunDiscovery) {
    Write-Host "Retrieving CM Application List."
    $CMAppListQuery = "SELECT * FROM SMS_Application
        WHERE SMS_Application.IsLatest = 'True' 
        AND SMS_Application.IsEnabled = 'True' 
        AND SMS_Application.IsExpired = 'False'"

    $CMAppList = $null
    $CMAppList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query $CMAppListQuery
}

# Get list of Configuration Items which includes various Rules
if (!$CMConfigurationItemList -or $RerunDiscovery) {
    $Global:CMConfigurationItemList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ConfigurationItem"
}

# Get list of Global Conditions which includes various custom Rules
if (!$CMGlobalConditionList -or $RerunDiscovery) {
    $Global:CMGlobalConditionList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_GlobalCondition"
}

# Get list of Categories which includes App and User Categories
if (!$CategoryList -or $RerunDiscovery) {
    $Global:CategoryList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_CategoryInstance"
    $Global:UserCategories = $CategoryList | Where-Object { $_.CategoryTypeName -eq 'CatalogCategories' }
    $Global:AppCategories = $CategoryList | Where-Object { $_.CategoryTypeName -eq 'AppCategories' }
}

# Retrieve Distribution Point info
if (!$DistributionPoint -or $RerunDiscovery) {
    $Global:DistributionPoint = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_DistributionPointInfo WHERE Name IN ( '$CMServer' )"
}

# Retrieve list of Collections
if (!$CollectionList -or $RerunDiscovery) {
    $Global:CollectionList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_Collection"
}

if (!$DetectionName -or $RerunDiscovery) {
    $Global:DetectionName = $AppTemplateName
}

# Initialize connection manager and connect
if (!$ConnectionManager) {
    $Global:ConnectionManager = [Microsoft.ConfigurationManagement.ManagementProvider.WqlQueryEngine.WqlConnectionManager]::new()
    $null = $ConnectionManager.Connect($CMServer)
}

$Global:AppSoftwareVersion = $NewVersion
if (($AppSoftwareVersion -eq 'Latest') -and ($DisableCurrentApplicationCheck)) {
    $Global:AppSoftwareVersion = (Get-CMAFApplicationUpdatedVersions -AppTemplatePath $AppTemplatePath -DisableCurrentApplicationCheck -LogFile $LogFile).NewestVersion
} elseif ($AppSoftwareVersion -eq 'Latest') {
    $Global:AppSoftwareVersion = (Get-CMAFApplicationUpdatedVersions -AppTemplatePath $AppTemplatePath -LogFile $LogFile).NewestVersion
}

if (Test-Path -Path "$XmlDirectory\$AppTemplateName.ico" -ErrorAction SilentlyContinue) {
    $Global:IconLocationFile = "$XmlDirectory\$AppTemplateName.ico"
} else {
    $Global:IconLocationFile = $null
}

$Global:AppXMLContent = (Get-Content -Path $AppTemplatePath)
$Global:AppXML = [xml]$AppXMLContent

$Global:AppPublisher = $AppXML.Application.DisplayInfo.Publisher
$Global:AppAppCategories = $AppXML.Application.DisplayInfo.SoftwareCategories
$Global:AppUserCategories = $AppXML.Application.DisplayInfo.UserCategories

#region -----------------------------------------------------------[Format Application Name]------------------------------------------------------------
# Make special consideration for Applications with a suffix such as 'for Faculty and Staff' or 'for Labs', etc.
$AppNameSuffix = $AppXML.Application.DisplayInfo.Suffix

if ($AppNameSuffix) {
    $appBaseName = $AppTemplateName.Replace($AppNameSuffix, '')
    $appBaseName = $appBaseName.Remove($appBaseName.Length - 1)
    $appName = "$appBaseName $AppSoftwareVersion $AppNameSuffix"
    $appFriendlyName = "$appBaseName $AppNameSuffix"
} else {
    $appBaseName = $AppTemplateName
    $appName = "$appBaseName $AppSoftwareVersion"
    $appFriendlyName = $appBaseName
}

if ($AppXML.Application.DisplayInfo.Custom.TitleLogic) {
    $TempArch = $null
    $TempArch = $AppXML.Application.DeploymentType.Architecture | Select-Object -First 1
    $TempXML = [xml]($AppXMLContent -replace ('{SoftwareVersion}', $AppSoftwareVersion) -replace ('{Architecture}', $TempArch))

    $appNameParts = foreach ($item in $TempXML.Application.DisplayInfo.Custom.TitleLogic.Item) {
        if (!$item.Attributes) {
            $item
        } else {
            if ($item.Script) {
                $x = (Get-Item -Path $item.TargetItemPath)
                $ScriptString = $item.Script -replace ('{THIS}', '$x') -replace ('{TargetItemProperty}', $item.TargetItemProperty)
                #$encodedcommand = [Convert]::ToBase64String([Text.Encoding]::Unicode.GetBytes($string))

                $ScriptResult = [scriptblock]::Create($ScriptString)
                $ScriptResult.Invoke()
            }
        }
    }

    $appName = $appNameParts -join ' '
    $appFriendlyName = $appName
}

#endregion -----------------------------------------------------------[END Format Application Name]------------------------------------------------------------

$AppCurrent = $null
$AppCurrentWMI = $null
$AppCurrentDeserializedXML = $null
$AppCurrentAppCategoryList = $null
$AppCurrentUserCategoryList = $null
$AppCurrentDeploymentList = $null

#region -----------------------------------------------------------[Get Most Current Deployed Version of Application]------------------------------------------------------------
# Retreive the most current deployed version of the application
if ($DisableCurrentApplicationCheck) {
    $AppCurrent = $null
} else {
    if ($AppTemplateName -eq 'Java') {
        $AppCurrent = $CMAppList | Where-Object { ($_.LocalizedDisplayName -like "$AppBaseName*$AppNameSuffix") -and ($_.LocalizedDisplayName -notlike "$AppBaseName SE Development Kit*$AppNameSuffix") } |
        Sort-Object -Property SoftwareVersion -Descending | Select-Object -First 1
    } else {
        $AppCurrent = $CMAppList | Where-Object { ($_.LocalizedDisplayName -like "*$AppBaseName*$AppNameSuffix") } |
        Sort-Object -Property SoftwareVersion -Descending | Select-Object -First 1
    }
}
#endregion -----------------------------------------------------------[END Get Most Current Deployed Version of Application]------------------------------------------------------------

#region -----------------------------------------------------------[Manage Application Categories]------------------------------------------------------------

if ($CopyCategories) {
    if ($AppCurrent) {
        # If a current version of the application is deployed, deserialize its XML to get information
        $AppCurrentWMI = [wmi]"\\$CMServer\root\sms\site_$($CMSite):SMS_Application.CI_ID=$($AppCurrent.CI_ID)"
        $AppCurrentDeserializedXML = [Microsoft.ConfigurationManagement.ApplicationManagement.Serialization.SccmSerializer]::DeserializeFromString($AppCurrentWMI.SDMPackageXML)

        # Get App Categories
        $AppAppCategories = [pscustomobject]@{
            Category = [System.Collections.Generic.List[string]]::new()
        }
        if ($AppCurrent.LocalizedCategoryInstanceNames) {        
            foreach ($LocalizedCategoryInstanceName in $AppCurrent.LocalizedCategoryInstanceNames) {
                $AppAppCategories.Category.Add($LocalizedCategoryInstanceName)
            }
        }

        # Get User Categories
        $AppUserCategories = [pscustomobject]@{
            Category = [System.Collections.Generic.List[string]]::new()
        }
        if ($AppCurrentDeserializedXML.DisplayInfo[0].userCategories) {       
            foreach ($UserCategory in $AppCurrentDeserializedXML.DisplayInfo[0].userCategories) {
                $AppUserCategories.Category.Add(($UserCategories | Where-Object { $_.CategoryInstance_UniqueID -eq $UserCategory }).LocalizedCategoryInstanceName)
            }
        }
    }

    # Make sure Pre-Approved exists in both the Application and User Categories.
    if (('Pre-Approved' -in $AppAppCategories.Category) -and ('Pre-Approved' -notin $AppUserCategories.Category)) {
        $AppUserCategories.Category.Add('Pre-Approved')
    }

    if (('Pre-Approved' -in $AppUserCategories.Category) -and ('Pre-Approved' -notin $AppAppCategories.Category)) {
        $AppAppCategories.Category.Add('Pre-Approved')
    }
}
#>
#endregion -----------------------------------------------------------[END Manage Application Categories]------------------------------------------------------------

#region -----------------------------------------------------------[Manage Application Deployments]------------------------------------------------------------
# Get Deployment information for the most current deployed version of the application.
if ($CopyDeployments) {        
    $AppCurrentDeploymentList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ApplicationAssignment WHERE ApplicationName = `"$($AppCurrent.LocalizedDisplayName)`"" 
} else {
    # If CopyDeployments is not specified, grab deployment information from Dummy Application which is deployed as 
    # Available to the '[SWU] User Self Service Test - Available | Install' collection.
    $AppCurrentDeploymentList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ApplicationAssignment WHERE ApplicationName = 'Dummy Application'"
}
#endregion -----------------------------------------------------------[END Manage Application Deployments]------------------------------------------------------------

#region -----------------------------------------------------------[Format/Parse Application Deployment Types]------------------------------------------------------------
$AppDTCol = foreach ($AppDT in $AppXML.Application.DeploymentType) {
    New-CMAFDeploymentTypeObject -AppDT $AppDT -AppFriendlyName $AppFriendlyName -AppSoftwareVersion $AppSoftwareVersion
}
#endregion -----------------------------------------------------------[END Format/Parse Application Deployment Types]------------------------------------------------------------

if ($ScriptOutputTest) {
    Write-Host "ScriptOutputTest switch was specified. Ending script."
    return
}

# Make sure there the Deployment Type collection is not empty
if ($AppDTCol.Count -eq 0) {
    Write-Host "The Deployment Type collection is empty. Ending script." -ForegroundColor Red
    return
}

## MAIN LOGIC

Try {
    if ($CMAppList | Where-Object { $_.LocalizedDisplayName -eq $AppName }) {
        Throw "This application already exists. Exiting."
    } elseif ($AppCurrent.SoftwareVersion -eq $AppSoftwareVersion) {
        Throw "The current version is the same as the input version. Exiting."
    } else {

        Write-Log -Path $LogFile -Level Info -Message "Starting Script: $ScriptName."
        
        Write-Log -Path $LogFile -Level Info -Message "The script was started with the following input parameters:
        AppTemplatePath       = $AppTemplatePath
        NewVersion       = $AppSoftwareVersion
        Architecture     = $Architecture
        DetectionName    = $DetectionName
        CopyDeployments  = $CopyDeployments"
    

        # Initialize the new AppFab object
        $NewAppFab = $null
        $NewAppFab = [pscustomobject]@{
            Name                     = $appName
            Publisher                = $AppPublisher
            SoftwareVersion          = $AppSoftwareVersion
            AutoInstall              = $true
            LocalizedName            = $appFriendlyName
            ReleaseDate              = $AppReleaseDate
            Icon                     = $null
            DeploymentTypes          = $null
            DistributionPointSetting = 'AutoDownload'
            AppCategories            = $AppAppCategories
            UserCategories           = $AppUserCategories
        }

        $AppFabDTList = [System.Collections.Generic.List[pscustomobject]]::new()

        foreach ($appDT in $appDTCol) {
            $appDTTitle = $appDT.appDTTitle
            $appDTArchitecture = $appDT.appDTArchitecture
            $appDTTechnology = $appDT.appDTTechnology
            $appDTDetectionMethod = $appDT.appDTDetectionMethod
            $appDTContentLocation = $appDT.appDTContentLocation
            $appDTInstallCommand = $appDT.appDTInstallCommand
            $appDTUninstallCommand = $appDT.appDTUninstallCommand
            $appDTRepairCommand = $appDT.appDTRepairCommand
            $appDTScriptRunAs32Bit = $appDT.appDTScriptRunAs32Bit
            $appDTScriptLanguage = $appDT.appDTScriptLanguage
            $appDTScriptText = $appDT.appDTScriptText
            $AppDTExecutionContext = $AppDT.AppDTExecutionContext
            $appDTDependency = $appDT.appDTDependency     
            $appDTRequirements = $appDT.appDTRequirements
            $AppDTSupersedes = $AppDT.Supersedes

            Write-Host "ExecutionContext: $($AppDT.AppDTExecutionContext)"
            Write-Host "ExecutionContext: $AppDTExecutionContext"

            # Create Application Deployment Type
            Write-Log -Path $LogFile -Level Info -Message "Adding new Deployment Type to the application with the following properties:
            ApplicationName          = $appName
            ContentLocation          = $appDTContentLocation
            DeploymentTypeName       = $appDTTitle
            InstallCommand           = $appDTInstallCommand
            UninstallCommand         = $appDTUninstallCommand
            LogonRequirementType     = 'WhetherOrNotUserLoggedOn'
            ScriptLanguage           = $appDTScriptLanguage
            ScriptText               = $appDTScriptText
            UserInteractionMode      = 'Hidden'
            InstallationBehaviorType = 'InstallForSystem'
            RebootBehavior           = 'NoAction'"

            $NewAppFabDT = [pscustomobject]@{
                ApplicationName          = $appName
                ContentLocation          = $appDTContentLocation
                DeploymentTypeName       = $appDTTitle
                InstallCommand           = $appDTInstallCommand
                UninstallCommand         = $appDTUninstallCommand
                RepairCommand            = $appDTRepairCommand
                LogonRequirementType     = 'WhetherOrNotUserLoggedOn'
                ScriptLanguage           = $appDTScriptLanguage
                ScriptText               = $appDTScriptText
                UserInteractionMode      = 'Hidden'
                InstallationBehaviorType = 'InstallForSystem'
                RebootBehavior           = 'NoAction'
                ExecutionContext         = $AppDTExecutionContext
                Dependencies             = $null
                Requirements             = $null
                Supersedes               = $null
            }
            

            if ($AppDT.appDTDependency) {
                $DTDependencies = $null
                $DTDependencies = New-CMAFDeploymentTypeDependencyObject -AppDT $AppDT
                $NewAppFabDT.Dependencies = $DTDependencies
            }

            if ($AppDT.appDTRequirements) {
                $DTRequirements = $null
                $DTRequirements = New-CMAFDeploymentTypeRequirementObject -AppDT $AppDT
                $NewAppFabDT.Requirements = $DTRequirements
            }            

            if ($AppDT.AppDTSupersedes) {
                $DTSupersedes = $null
                $DTSupersedes = New-CMAFDeploymentTypeSupersedenceObject -AppDT $AppDT
                $NewAppFabDT.Supersedes = $DTSupersedes
            }

            # Add the Deployment Type to the Deployment Type List
            $null = $AppFabDTList.Add($NewAppFabDT)

        } # END foreach App DeploymentType 
        
        # Add Deployment Types
        $NewAppFab.DeploymentTypes = $AppFabDTList

        # Add Icon
        if ($IconLocationFile) {            
            $IconPath = (Get-Item -Path $IconLocationFile).FullName
            $IconBmp = [System.Drawing.Bitmap]::FromFile($IconPath)
            
            if (($IconBmp.Width -gt 512) -or ($IconBmp.Height -gt 512)) {
                $IconSize = 512
            } else {
                $IconSize = $iconBmp.Width
            }
            
            $NewBmp = New-Object -TypeName System.Drawing.Bitmap -ArgumentList $iconBmp, $IconSize, $IconSize
            $NewIconPath = "$($env:Temp)\$($AppDisplayInfo.Title)_$($IconSize).png"
            $NewBmp.Save($NewIconPath, [System.Drawing.Imaging.ImageFormat]::Png)
            $NewBmp.Dispose()

            $Icon = [Microsoft.ConfigurationManagement.ApplicationManagement.Icon]::new()
            $Icon.Data = [System.IO.File]::ReadAllBytes($NewIconPath)

            $NewAppFab.Icon = $Icon
        }

        # Create the Application
        $null = New-CMAFApplication -AppFab $NewAppFab

        # Retrieve CM Application info for the newly created Application
        $CMTargetApp = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ApplicationLatest WHERE LocalizedDisplayName = `"$($NewAppFab.Name)`""

        # Distribute Content
        # Retrieve Content Package info for the newly created Application
        $ContentPackage = $null
        $ContentPackage = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ContentPackage WHERE SecurityKey = `"$($CMTargetApp.ModelName)`""

        # Distribute Content to Distribution Points
        $null = Invoke-CimMethod -InputObject $ContentPackage -MethodName 'AddDistributionPoints' -Arguments (@{
                SiteCode = @($CMSite); 
                NALPath  = @("$($DistributionPoint.NALPath)")
            })

        # Deploy Application
        Write-Log -Path $LogFile -Level Info -Message "Starting Application Deployment."
        #$Distributed = $false
        foreach ($AppDeployment in $AppCurrentDeploymentList) {
            #$appDeploymentCollection = $AppDeployment.CollectionName
            


            <#


            switch ($AppDeployment.OverrideServiceWindows) {
                'False' {$OverrideServiceWindow = $false}
                'True' {$OverrideServiceWindow = $true}
            }

            switch ($AppDeployment.RequireApproval) {
                'False' {$ApprovalRequired = $false}
                'True' {$ApprovalRequired = $true}
            }

            switch ($AppDeployment.UserUIExperience) {
                'False' {$UserNotification = 'HideAll'}
                'True' {$UserNotification = 'DisplaySoftwareCenterOnly'}
            }

            if ($AppDeployment.NotifyUser -eq $true) {
                $UserNotification = 'DisplayAll'
            }

            switch ($AppDeployment.RebootOutsideOfServiceWindows) {
                'False' {$RebootOutsideServiceWindow = $false}
                'True' {$RebootOutsideServiceWindow = $true}
            }
            
            Write-Log -Path $LogFile -Level Info -Message "Creating the following Application Deployment:
            Name                       = $appName
            ApprovalRequired           = $ApprovalRequired
            DeployAction               = $DeployAction
            DeployPurpose              = $DeployPurpose
            OverrideServiceWindow      = $OverrideServiceWindow
            PreDeploy                  = $false
            RebootOutsideServiceWindow = $RebootOutsideServiceWindow
            TimeBaseOn                 = 'LocalTime'
            UserNotification           = $UserNotification
            CollectionName             = $appDeploymentCollection
            DistributionPointName      = $CMServer
            AvailableDateTime          = $(Get-Date)"   
            
            $ApplicationDeploymentPropertyList = @{
                Name                       = $appName
                ApprovalRequired           = $ApprovalRequired
                DeployAction               = $DeployAction
                DeployPurpose              = $DeployPurpose
                OverrideServiceWindow      = $OverrideServiceWindow
                PreDeploy                  = $false
                RebootOutsideServiceWindow = $RebootOutsideServiceWindow
                TimeBaseOn                 = 'LocalTime'
                UserNotification           = $UserNotification
                CollectionName             = $appDeploymentCollection
                DistributionPointName      = $CMServer
                AvailableDateTime          = $(Get-Date)
            }
            #>

            # Deploy Application
            #$NewApplicationAssignment = $ConnectionManager.CreateInstance("SMS_ApplicationAssignment")     

            $CreationTime = Get-Date

            $DeploymentPropertyList = @{ }

            foreach ($AppDeploymentProperty in ($AppDeployment.PSBase.CimInstanceProperties | Where-Object { ($_.Flags -notmatch 'NullValue') -and ($_.Name -notmatch 'ApplicationName|AppModelID|AssignmentDescription|AssignedCI_UniqueID|AssignedCIs|AssignmentID|AssignmentName|AssignmentUniqueID|CreationTime|EnforcementDeadline|LastModificationTime|LastModifiedBy|StartTime') })) {
                $DeploymentPropertyList.Add("$($AppDeploymentProperty.Name)", $($AppDeploymentProperty.Value))    
            }

            switch ($AppDeployment.DesiredConfigType) {
                1 { $DeployAction = 'Install' }
                2 { $DeployAction = 'Uninstall' }
            }

            $DeploymentPropertyList.Add('ApplicationName', $($CMTargetApp.LocalizedDisplayName))
            $DeploymentPropertyList.Add('AppModelID', $($CMTargetApp.CI_ID))
            $DeploymentPropertyList.Add('AssignedCI_UniqueID', $($CMTargetApp.CI_UniqueID))
            $DeploymentPropertyList.Add('AssignedCIs', @($($CMTargetApp.CI_ID)))
            $DeploymentPropertyList.Add('AssignmentName', "$($CMTargetApp.LocalizedDisplayName)_$($AppDeployment.CollectionName)_$($DeployAction)")
            $DeploymentPropertyList.Add('CreationTime', $CreationTime)
            $DeploymentPropertyList.Add('StartTime', $CreationTime)

            switch ($AppDeployment.OfferTypeID) {
                0 {
                    $DeployPurpose = 'Required'
                    $DeploymentPropertyList.Add('EnforcementDeadline', $CreationTime)
                }
                2 {
                    $DeployPurpose = 'Available'
                }
            }

            <#
            OfferFlags - UInt32
    
            Qualifiers: bits, CIMTYPE, not_null
            
            [Description Not Available]
            
            Possible Bit Values: 
            PREDEPLOY(1)
            ONDEMAND(2)
            ENABLEPROCESSTERMINATION(4)
            ALLOWUSERSTOREPAIRAPP(8)
            RELATIVESCHEDULE(16)
            #>
            if (!$DisallowRepair) {
                if (($DeploymentPropertyList["OfferFlags"] -lt 8) -or (($DeploymentPropertyList["OfferFlags"] -ge 16) -and ($DeploymentPropertyList["OfferFlags"] -le 23))) {
                    $DeploymentPropertyList["OfferFlags"] += 8
                }                
            } else {
                if ((($DeploymentPropertyList["OfferFlags"] -gt 8) -and ($DeploymentPropertyList["OfferFlags"] -lt 16)) -or ($DeploymentPropertyList["OfferFlags"] -ge 24)) {
                    $DeploymentPropertyList["OfferFlags"] -= 8
                }
            }

            $null = New-CMAFApplicationDeployment @DeploymentPropertyList

        } # END foreach Deployment in Current Deployments
    } # END If ($AppCurrent.SoftwareVersion -eq $AppSoftwareVersion)
    
} Catch {
    #Write-Log -Path $LogFile -Level Error -Message "Error: $($_.Exception)"
    #Write-Host "ERROR Pop-Location: $(Pop-Location -PassThru)"

    Write-Log -Path $LogFile -Level Error -Message "Error: $($pscmdlet.ThrowTerminatingError($PSItem))"
    #Write-Log -Path $LogFile -Level Error -Message "Error: $($pscmdlet.ThrowTerminatingError($PSItem.Exception))"

    #continue
}
if ($pscmdlet.ShouldProcess("Target of action", "Action will happen")) {
    #do action
} else {
    #don't do action but describe what would have been done
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
    Write-Host 'Completed Successfully.' -ForegroundColor Green
    #Write-Host "Pop-Location: $(Pop-Location -PassThru)"

    if ($RetireOld) {
        Write-Log -Path $LogFile -Level Warn -Message "RetireOld flag specified. Retiring $($AppCurrent.LocalizedDisplayName)."
        .\Retire-Application.ps1 -retiringAppNames $($AppCurrent.LocalizedDisplayName)
    }
}
#endregion