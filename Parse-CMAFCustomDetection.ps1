function Parse-CustomDetectionVersionLogic {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $AppDT,
        [Parameter(Mandatory = $true)]
        [string]$AppSoftwareVersion
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $DetectionVersionFilePath = $null
    $Local:DetectionVersion = $null
    

    $DetectionVersionFilePath = $AppDT.Custom.DetectionVersionLogic.TargetItemPath -replace ('{SoftwareVersion}', $AppSoftwareVersion) -replace ('{Architecture}', $AppDT.Architecture) -replace ('{DetectionName}', $DetectionName)
    if(Test-Path -Path $DetectionVersionFilePath) {
        if ($AppDT.Custom.DetectionVersionLogic.SCRIPT) {

            $appDTDetectionVersionLogicScriptText = $AppDT.Custom.DetectionVersionLogic.SCRIPT
            if($appDTDetectionVersionLogicScriptText) {   
                $appDTDetectionVersionLogicScriptText = $appDTDetectionVersionLogicScriptText -replace ('{DetectionVersionFilePath}',$DetectionVersionFilePath) -replace ('{DetectionName}', $DetectionName)
                $DetectionVersion = ([scriptblock]::Create($appDTDetectionVersionLogicScriptText).Invoke($PSScriptRoot,$appDT,$AppSoftwareVersion,$DetectionVersionFilePath)).Trim() # Pass $PSScriptRoot as a parameter
            }
        } else {
            if ($AppDT.Custom.DetectionVersionLogic.TargetItemType -eq 'EXE') {            
                $DetectionVersion = (Get-Item -Path $DetectionVersionFilePath).VersionInfo.$($AppDT.Custom.DetectionVersionLogic.TargetItemProperty).ToString().Trim()
            }
    
            if ($AppDT.Custom.DetectionVersionLogic.TargetItemType -match 'MSI|MSP') {
            
                [System.IO.FileInfo]$Path = Get-Item -Path $DetectionVersionFilePath
                ## "ProductCode", "ProductVersion", "ProductName", "Manufacturer", "ProductLanguage", "FullVersion" ##
    
                ## Get MSI Property
                $Property = $AppDT.Custom.DetectionVersionLogic.TargetItemProperty
    
                # Read property from MSI database
                $WindowsInstaller = New-Object -ComObject WindowsInstaller.Installer

                $MSIDatabase = $null
                $Query = $null
                switch ($appDT.Custom.DetectionVersionLogic.TargetItemType) {
                    'MSI' {
                        $MSIDatabase = $WindowsInstaller.GetType().InvokeMember("OpenDatabase", "InvokeMethod", $null, $WindowsInstaller, @($Path.FullName, 0))
                        $Query = "SELECT Value FROM Property WHERE Property = '$($Property)'"
                    }
                    'MSP' {
                        $MSIDatabase = $WindowsInstaller.GetType().InvokeMember("OpenDatabase", "InvokeMethod", $null, $WindowsInstaller, @($Path.FullName, 32))
                        $Query = "SELECT Value FROM MsiPatchMetadata WHERE Property = '$($Property)'"
                    }
                }
                
                $View = $MSIDatabase.GetType().InvokeMember("OpenView", "InvokeMethod", $null, $MSIDatabase, ($Query))
                $View.GetType().InvokeMember("Execute", "InvokeMethod", $null, $View, $null)
                $Record = $View.GetType().InvokeMember("Fetch", "InvokeMethod", $null, $View, $null)
                $PropertyResult = $Record.GetType().InvokeMember("StringData", "GetProperty", $null, $Record, 1)    
    
                # Commit database and close view
                $MSIDatabase.GetType().InvokeMember("Commit", "InvokeMethod", $null, $MSIDatabase, $null)
                $View.GetType().InvokeMember("Close", "InvokeMethod", $null, $View, $null)           
                $MSIDatabase = $null
                $View = $null
                $null = [System.Runtime.Interopservices.Marshal]::ReleaseComObject($WindowsInstaller)
    
                $DetectionVersion = $PropertyResult.Trim()
            }
        }

        
        <#
        if ($AppDT.Custom.DetectionVersionLogic.TargetItemType -eq 'Zip') {

            $appDTDetectionVersionLogicScriptText = $AppDT.Custom.DetectionVersionLogic.SCRIPT
            if($appDTDetectionVersionLogicScriptText) {   
                $appDTDetectionVersionLogicScriptText = $appDTDetectionVersionLogicScriptText -replace ('{DetectionVersionFilePath}',$DetectionVersionFilePath)
                $DetectionStatement = [scriptblock]::Create($appDTDetectionVersionLogicScriptText).Invoke()
            }
        }
        #>
        <#
        if ($AppDT.Custom.DetectionVersionLogic.TargetItemType -eq '7z') {

            $appDTDetectionVersionLogicScriptText = $AppDT.Custom.DetectionVersionLogic.SCRIPT
            if($appDTDetectionVersionLogicScriptText) {   
                $appDTDetectionVersionLogicScriptText = $appDTDetectionVersionLogicScriptText -replace ('{DetectionVersionFilePath}',$DetectionVersionFilePath) -replace ('{DetectionName}', $DetectionName)
                #Write-Host "Invoking Custom Version Logic Script" -ForegroundColor Yellow
                #Write-Host "DetectionVersion: $DetectionVersion"
                #Write-Host $appDTDetectionVersionLogicScriptText
                $DetectionVersion = ([scriptblock]::Create($appDTDetectionVersionLogicScriptText).Invoke($PSScriptRoot)).Trim() # Pass $PSScriptRoot as a parameter
                #Write-Host "Finished running Custom Version Logic Script. Output: $DetectionVersion" -ForegroundColor Yellow
            }
        }
        #>
    }

    # $DetectionVersion | Out-File -FilePath "$PSScriptRoot\Logs\$($CmdletName)_$($AppName).txt" -Force -Append -ErrorAction Stop # debugging purposes
    Write-Host "END $CmdletName" -ForegroundColor Green
    
    $DetectionVersion    
}

function Parse-CustomDetectionScriptLogic {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $AppDT,
        [Parameter(Mandatory = $true)]
        [string]$AppSoftwareVersion,
        [Parameter(Mandatory = $true)]
        [string]$appDTRegPath,
        [Parameter(Mandatory = $true)]
        [string]$DetectionVersion
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $DetectionScriptFilePath = $null
    $DetectionStatement = $null
    $AppArchitecture = $AppDT.Architecture

    $DetectionScriptFilePath = $appDT.Custom.DetectionScriptLogic.TargetItemPath -replace ('{SoftwareVersion}', $AppSoftwareVersion) -replace ('{Architecture}', $AppDT.Architecture)

    $appDTDetectionScriptLogicScriptText = $appDT.Custom.DetectionScriptLogic.SCRIPT
    if($appDTDetectionScriptLogicScriptText) {   
        $appDTDetectionScriptLogicScriptText = $appDTDetectionScriptLogicScriptText -replace ('{DetectionScriptFilePath}',$DetectionScriptFilePath) -replace ('{Architecture}', $AppDT.Architecture) -replace ('{DetectionRegPath}', $appDTRegPath)
        $DetectionStatement = [scriptblock]::Create($appDTDetectionScriptLogicScriptText).Invoke($PSScriptRoot,$appDT,$AppSoftwareVersion,$DetectionScriptFilePath) # Pass $PSScriptRoot as a parameter 
    }

    <#
    if ($appDT.Custom.DetectionScriptLogic.TargetItemType -eq 'Zip') {
        $appDTDetectionScriptLogicScriptText = $appDT.Custom.DetectionScriptLogic.SCRIPT
        if($appDTDetectionScriptLogicScriptText) {   
            $appDTDetectionScriptLogicScriptText = $appDTDetectionScriptLogicScriptText -replace ('{DetectionScriptFilePath}',$DetectionScriptFilePath) -replace ('{Architecture}', $AppDT.Architecture) -replace ('{DetectionRegPath}', $appDTRegPath)
            $DetectionStatement = [scriptblock]::Create($appDTDetectionScriptLogicScriptText).Invoke($PSScriptRoot)
        }
    }

    if ($appDT.Custom.DetectionScriptLogic.TargetItemType -eq 'JSON') {
        $appDTDetectionScriptLogicScriptText = $appDT.Custom.DetectionScriptLogic.SCRIPT
        if($appDTDetectionScriptLogicScriptText) {   
            $appDTDetectionScriptLogicScriptText = $appDTDetectionScriptLogicScriptText -replace ('{DetectionScriptFilePath}',$DetectionScriptFilePath) -replace ('{Architecture}', $AppDT.Architecture) -replace ('{DetectionRegPath}', $appDTRegPath)
            # $appDTDetectionScriptLogicTargetItemPath = $appDT.Custom.DetectionScriptLogic.TargetItemPath 
            #Write-Host "appDT Custom Script Logic: $($appDTDetectionScriptLogicScriptText)"
            #$AppDT
            #Write-Host "AppSoftwareVersion: $AppSoftwareVersion"
            #Write-Host "PSScriptRoot: $PSScriptRoot"
            $DetectionStatement = [scriptblock]::Create($appDTDetectionScriptLogicScriptText).Invoke($appDT, $AppSoftwareVersion, $PSScriptRoot, $DetectionScriptFilePath) # Pass $PSScriptRoot as a parameter 
        }
    }

    if ($appDT.Custom.DetectionScriptLogic.TargetItemType -eq 'Directory') {
        $appDTDetectionScriptLogicScriptText = $appDT.Custom.DetectionScriptLogic.SCRIPT
        if($appDTDetectionScriptLogicScriptText) {   
            $appDTDetectionScriptLogicScriptText = $appDTDetectionScriptLogicScriptText -replace ('{DetectionScriptFilePath}',$DetectionScriptFilePath) -replace ('{Architecture}', $AppDT.Architecture) -replace ('{DetectionRegPath}', $appDTRegPath) -replace ('{DetectionVersion}', $DetectionVersion)
            $DetectionStatement = [scriptblock]::Create($appDTDetectionScriptLogicScriptText).Invoke($PSScriptRoot,$appDT,$AppSoftwareVersion,$DetectionScriptFilePath) # Pass $PSScriptRoot as a parameter 
        }
    }
    #>
    
    # $DetectionStatement | Out-File -FilePath "$PSScriptRoot\Logs\$CmdletName_$($appDT.Title).txt" -Force -Append # debugging purposes
    Write-Host "END $CmdletName" -ForegroundColor Green

    $DetectionStatement
}

function Parse-CustomDetectionNameLogic {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        $AppDT,
        [Parameter(Mandatory = $true)]
        [string]$AppSoftwareVersion
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $DetectionNameFilePath = $null
    $DetectionName = $null

    $DetectionNameFilePath = $appDT.Custom.DetectionNameLogic.TargetItemPath -replace ('{SoftwareVersion}', $AppSoftwareVersion) -replace ('{Architecture}', $AppDT.Architecture)

    if ($AppDT.Custom.DetectionNameLogic.SCRIPT) {

        $appDTDetectionNameLogicScriptText = $AppDT.Custom.DetectionNameLogic.SCRIPT
        if($appDTDetectionNameLogicScriptText) {   
            $appDTDetectionNameLogicScriptText = $appDTDetectionNameLogicScriptText -replace ('{DetectionNameFilePath}',$DetectionNameFilePath) -replace ('{DetectionVersion}', $DetectionVersion)
            $DetectionName = ([scriptblock]::Create($appDTDetectionNameLogicScriptText).Invoke($PSScriptRoot,$appDT,$AppSoftwareVersion,$DetectionNameFilePath)).Trim() # Pass $PSScriptRoot as a parameter
        }
    } else {
        if ($appDT.Custom.DetectionNameLogic.TargetItemType -eq 'EXE') {
            $DetectionName = (Get-Item -Path $DetectionNameFilePath).VersionInfo.$($appDT.Custom.DetectionNameLogic.TargetItemProperty).ToString().Trim()
        }
    
        if ($appDT.Custom.DetectionNameLogic.TargetItemType -match 'MSI|MSP') {
            
            [System.IO.FileInfo]$Path = Get-Item -Path $DetectionNameFilePath
            ## "ProductCode", "ProductVersion", "ProductName", "Manufacturer", "ProductLanguage", "FullVersion" ##
    
            ## Get MSI Property
            $Property = $appDT.Custom.DetectionNameLogic.TargetItemProperty
    
            # Read property from MSI database
            $WindowsInstaller = New-Object -ComObject WindowsInstaller.Installer

            $MSIDatabase = $null
            $Query = $null
            switch ($appDT.Custom.DetectionNameLogic.TargetItemType) {
                'MSI' {
                    $MSIDatabase = $WindowsInstaller.GetType().InvokeMember("OpenDatabase", "InvokeMethod", $null, $WindowsInstaller, @($Path.FullName, 0))
                    $Query = "SELECT Value FROM Property WHERE Property = '$($Property)'"
                }
                'MSP' {
                    $MSIDatabase = $WindowsInstaller.GetType().InvokeMember("OpenDatabase", "InvokeMethod", $null, $WindowsInstaller, @($Path.FullName, 32))
                    $Query = "SELECT Value FROM MsiPatchMetadata WHERE Property = '$($Property)'"
                }
            }
            
            $View = $MSIDatabase.GetType().InvokeMember("OpenView", "InvokeMethod", $null, $MSIDatabase, ($Query))
            $View.GetType().InvokeMember("Execute", "InvokeMethod", $null, $View, $null)
            $Record = $View.GetType().InvokeMember("Fetch", "InvokeMethod", $null, $View, $null)
            $PropertyResult = $Record.GetType().InvokeMember("StringData", "GetProperty", $null, $Record, 1)    
    
            # Commit database and close view
            $MSIDatabase.GetType().InvokeMember("Commit", "InvokeMethod", $null, $MSIDatabase, $null)
            $View.GetType().InvokeMember("Close", "InvokeMethod", $null, $View, $null)           
            $MSIDatabase = $null
            $View = $null
            $null = [System.Runtime.Interopservices.Marshal]::ReleaseComObject($WindowsInstaller)
    
            $DetectionName = $PropertyResult
        }
    }
    

    # $DetectionName | Out-File -FilePath "$PSScriptRoot\Logs\$CmdletName_$($appDT.Title).txt" -Force -Append # debugging purposes
    Write-Host "END $CmdletName" -ForegroundColor Green

    $DetectionName
}