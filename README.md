[[_TOC_]]

## Overview

The ConfigMgr Application Fabricator (CMAppFab for short) is a framework designed to streamline the application packaging and deployment process. CMAppFab consists of a set of PowerShell scripts and application templates. When a template is passed to the build script, a set of scripts is then triggered to build the ConfigMgr application according to the build template, deploy the newly created application to collections described in the application deployment template, and / or, optionally, retire the old version of the application, copy the old version's deployments to the new application, and manage dependencies.

## Components

The CMAppFab framework consists of the following components:
*  Build Scripts
*  Application Build Templates
*  Application Deployment Templates
*  Application Icons
*  Extra Files

#### Build Scripts
*  **Build-CMAFApplication.ps1** - This is the main script to invoke in order to build and deploy an application. This script is dependent on all other build scripts. This script accepts an application template as an input.
*  **New-CMAFDeploymentTypeObject.ps1** - This script parses the application template in order to build the Deployment Type object or objects which it will pass along into the next several steps. This step dynamically builds the application detection script based on various rules and template fields. If the application template has custom deployment type detection logic, the `Parse-CMAFCustomDetection` function from the `Parse-CMAFCustomDetection.ps1` script is invoked.
*  **Parse-CMAFCustomDetection.ps1** - This script parses custom deployment type detection logic present within an application template. Several types of custom deployment type detection types exist.
*  **New-CMAFApplication.ps1** - This script is responsible for creating the initial ConfigMgr Application using the input parameters present in the application template as well as the Deployment Type objects from the previous steps.
*  **New-CMAFDeploymentTypeDependency.ps1** - This script builds application dependencies if they are specified in the application template.
*  **New-CMAFDeploymentTypeRequirement.ps1** - This script builds application requirements if they are specified in the application template.
*  **New-CMAFDeploymentTypeSupersedence.ps1** - This script builds application supersedence rules if they are specified in the application template.

*  **New-CMAFApplicationDeployment.ps1** - This script creates application deployments for the newly created application.
*  **Get-CMAFApplicationUpdatedVersions.ps1** - This script checks if previous versions of the application are present within ConfigMgr. This is invoked if the `-DisableCurrentApplicationCheck` switch is not specified in `Build-CMAFApplication.ps1`.
*  **Retire-Application.ps1** - This script retires and moves or removes one or more applications. This script is invoked when specifying the `-RetireOld` switch.
*  **Load-ConfigMgrAssemblies.ps1** - This script loads the necessary DLLs and Modules in order to properly interact with ConfigMgr's libraries.

#### Application Build Templates
Each application packaged and deployed with **CMAppFab** must have a corresponding **application build template**. The template is an XML file which describes the application, the location of the installation source files, and the application deployment and detection properties. For more information about **application build templates** see [Application Build Templates](https://gitlab.com/max.bado/cmappfab/-/wikis/Application-Build-Templates).

#### Application Deployment Templates
Content pending.

#### Application Icons
Each application may have a corresponding icon. The icon must be in .ico format and can be up to 512x512 in size.

#### Extra Files
In order to correctly parse certain custom deployment type detection rules, CMAppFab uses 7-Zip command lines tools. 7z.exe and 7za.exe must exist in the CMAppFab root directory.

## Environment Requirements
**CMAppFab** relies on several assumptions about the environment in order to function properly:

#### Configuration Manager Console
**CMAppFab** requires that the Configuration Manager Console is installed on the machine from which CMAppFab is invoked. Extensive effort has been made to eliminate the dependency on the ConfigurationManager PowerShell Module. Instead of the Module, **CMAppFab** uses the ConfigMgr .NET libraries directly.

#### PowerShell Version
**CMAppFab** has been tested on Windows PowerShell 5.1, PowerShell Core 6.x, and PowerShell 7.x.

#### Application Installation Source Directory Structure
The framework assumes the following directory structure for application source files:
```
└───Google (Manufacturer)
    └───Chrome (Application Name)
        └───archive
            └───80.0.3987.122 (Application Version)
                ├───win_x64 (Application Architecture)
                │   ├───AppDeployToolkit
                │   ├───Files
                │   └───SupportFiles
                └───win_x86 (Application Architecture)
                    ├───AppDeployToolkit
                    ├───Files
                    └───SupportFiles
```

Currently, the only allowed architecture directories are `win_x86` and `win_x64`. All other architecture directories will be ignored.

## Usage

#### Script Parameters
*  **AppTemplatePath** - Path to the Application Template XML. The path can be absolute or relative.
*  **NewVersion** - New version of the application to be created. The version must exist in the application source path defined in the Application XML Template.
*  **RetireOld** - When specified, any previous version of the application currently existing within ConfigMgr will be retired via the `Retire-Application.ps1` script.
*  **DisableCurrentApplicationCheck** - When specified, the `Get-CMAFApplicationUpdateVersions` function is NOT executed. This prevents the usage of the following flags:
   *  **RetireOld**
   *  **CopyDeployments**
   *  **CopyCategories**
*  **ScriptOutputTest** - When specified, the application is NOT built. Instead, the compiled detection script is output to a file in the `.\ScriptText` directory.
  The output file name has the following form: `ScriptText_<Deployment Type Name>.txt`
  One file is output for each Deployment Type.
*  **DisallowRepair** - When specified, the Allow Repair flag is disabled for each application deployment created by the script.
*  **CopyCategories** - When specified, the script attempts to copy the User and Software Categories from the previous version of the application that exists within ConfigMgr.
  Categories specified in the application build template are ignored.
*  **CopyDeployments** - When specified, the script attempts to copy the deployments from the previous version of the application that exists within ConfigMgr.
*  **RerunDiscovery** - When specified, the script will rerun the ConfigMgr environment discovery. If this switch is not specified, the ConfigMgr environment is cached
  within the session.
*  **CMServer** - The ConfigMgr server name. If this is not specified, the script attempts to discover the server name automatically.
*  **CMSite** - The ConfigMgr site name. If this is not specified, the script attempts to discover the site name automatically.
*  **LogFile** - Set a custom Log File Path. If this is not set, the default Log location is `.\Logs\Build-Application_<ApplicationTemplateName>.log`.

#### Examples
See [Examples](https://gitlab.com/max.bado/cmappfab/-/wikis/Examples)
