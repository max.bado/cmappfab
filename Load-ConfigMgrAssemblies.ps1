function Load-ConfigMgrAssemblies {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$false)]
        [switch]$LoadModule
    )

    if (Test-Path -Path $ENV:SMS_ADMIN_UI_PATH -ErrorAction SilentlyContinue) {
        $AdminConsoleDirectory = Get-Item -Path "$($ENV:SMS_ADMIN_UI_PATH)\.."
    } elseif (Test-Path -Path "${env:ProgramFiles(x86)}\Microsoft Configuration Manager\AdminConsole\bin\" -ErrorAction SilentlyContinue) {
        $AdminConsoleDirectory = Get-Item -Path "${env:ProgramFiles(x86)}\Microsoft Configuration Manager\AdminConsole\bin\"
    } else {
        Write-Host "Unable to locate ConfigMgr assemblies directory. Ending script."
        break
    }

    # Get list of currently loaded assemblies
    $CurrentAssemblyList = [System.AppDomain]::CurrentDomain.GetAssemblies()

    # Load System.Windows.Forms.dll since it is not loaded by default by PowerShell Core
    if ('System.Windows.Forms.dll' -notin $CurrentAssemblyList.ManifestModule.Name) {
        Write-Host "Loading System.Windows.Forms.dll"   
        $TargetAssemblyPath = Get-Item -Path "$env:windir\Microsoft.NET\Framework64\*\System.Windows.Forms.dll" | Select-Object -Last 1
        $null = [Reflection.Assembly]::LoadFrom($TargetAssemblyPath.FullName)
    }

    # Load Configuration Manager Assemblies
    $filesToLoad = "Microsoft.ConfigurationManagement.ApplicationManagement.dll", 
        "AdminUI.WqlQueryEngine.dll", 
        "AdminUI.DcmObjectWrapper.dll", 
        "DcmObjectModel.dll", 
        "AdminUI.AppManFoundation.dll", 
        "AdminUI.WqlQueryEngine.dll", 
        "Microsoft.ConfigurationManagement.ApplicationManagement.Extender.dll", 
        "Microsoft.ConfigurationManagement.ManagementProvider.dll", 
        "Microsoft.ConfigurationManagement.ApplicationManagement.MsiInstaller.dll"
        
    Get-ChildItem -Path $AdminConsoleDirectory.FullName | Where-Object {($_.Name -in $filesToLoad) -and ($_.Name -notin $CurrentAssemblyList.ManifestModule.Name)} | ForEach-Object {
        $Module = $_
        Write-Host "Loading $($Module.Name)"    
        $null = [Reflection.Assembly]::LoadFrom($_.FullName)
    }

    if ($LoadModule) {
        # Import Configuration Manager Module
        if (!(Get-Module -Name 'ConfigurationManager')) {
            Import-Module "$AdminConsoleDirectory\ConfigurationManager.psd1"
        }
    }
}

