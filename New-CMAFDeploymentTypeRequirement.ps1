﻿function New-CMAFDeploymentTypeRequirementObject {
    [CmdletBinding()]
    Param(
        $AppDT
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    # Get list of Configuration Items which includes various Rules
    if (!$CMConfigurationItemList -or $RerunDiscovery) {
        $Global:CMConfigurationItemList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ConfigurationItem"
    }

    # Get list of Global Conditions which includes various custom Rules
    if (!$CMGlobalConditionList -or $RerunDiscovery) {
        $Global:CMGlobalConditionList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_GlobalCondition"
    }

    $DTRequirementsList = $AppDT.AppDTRequirements
    $RequirementsObjectList = [System.Collections.Generic.List[pscustomobject]]::new()
    $NoncomplianceSeverity = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.NoncomplianceSeverity]::None
    $RequirementOperator = $null

    foreach ($Rule in $DTRequirementsList.Rule) {        

        # Collection of Rule Expressions that will be added to the Rule
        $RuleExpressionCollection = $null
        $RuleExpressionCollection = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.RuleExpression]]::new()

        $RuleNameList = @()
        switch ($Rule.Type) {
            'Operating System' {
                $RuleExpressionCollection = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.RuleExpression]]::new()

                # Define the operator
                $RequirementOperator = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ExpressionOperators.ExpressionOperator]::"$($Rule.Operator)"

                foreach ($ExpressionItem in $Rule.Expression) {
            
                    # Create the Rule Expression and add it to the Rule Expression collection
                    $NewRequirementsRuleExpression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.RuleExpression]::new($ExpressionItem.ToString())
                    $null = $RuleExpressionCollection.Add($NewRequirementsRuleExpression)

                    $RuleNameList += ($CMConfigurationItemList | Where-Object { $_.ModelName -eq $ExpressionItem.ToString() }).LocalizedDisplayName
                }

                # Create the Operating System Expression comprised of the individual Rule Expressions
                $Expression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.OperatingSystemExpression]::new($RequirementOperator, $RuleExpressionCollection)
            }

            'Custom' {
                $RuleExpressionCollection = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.RuleExpression]]::new()

                # Define the operator
                $RequirementOperator = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ExpressionOperators.ExpressionOperator]::"$($Rule.Operator)"

                foreach ($ExpressionItem in $Rule.Expression) {
            
                    # Create the Rule Expression and add it to the Rule Expression collection
                    $NewRequirementsRuleExpression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.RuleExpression]::new($ExpressionItem.ToString())
                    $null = $RuleExpressionCollection.Add($NewRequirementsRuleExpression)

                    $RuleNameList += ($CMGlobalConditionList | Where-Object { $_.LocalizedDisplayName -eq $ExpressionItem.ToString() }).LocalizedDisplayName
                }

                # Create the Operating System Expression comprised of the individual Rule Expressions
                #$Expression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.OperatingSystemExpression]::new($RequirementOperator, $RuleExpressionCollection)
                $Expression = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DeploymentTypeDesiredState]::new($RequirementOperator, $RuleExpressionCollection)
            }

            <#
            'Custom' {

                $RuleExpressionCollection = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.ExpressionBase]]::new()

                $GlobalCondition = ($CMGlobalConditionList | Where-Object { $_.LocalizedDisplayName -eq $Rule.RuleName.Trim() })
                $ConfigurationItemSettings = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_ConfigurationItemSettings WHERE CI_UniqueID = '$($GlobalCondition.ModelName)'"

                $Operator = $Rule.Operator
                $Value = 'True'
                $SettingSourceType = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ConfigurationItemSettingSourceType]::GetName([Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ConfigurationItemSettingSourceType], $ConfigurationItemSettings.SourceType)

                $gcScope = $GlobalCondition.ModelName.Split("/")[0]
                $gcLogicalName = $GlobalCondition.ModelName.Split("/")[1]
                $gcDataType = $GlobalCondition.DataType
                $gcExpressionDataType = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DataType]::GetDataTypeFromTypeName($gcDataType)

                if ($operator.ToLower() -eq "notexistential" -OR $operator.ToLower() -eq "existential") {
                    $gcExpressionDataType = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.DataType]::Int64
                }
                #Retrieving logical name of setting 
                $settingsxml = [xml] ([wmi]$GlobalCondition.__PATH).SDMPackageXML
                if ($settingsxml.DesiredConfigurationDigest.GlobalSettings.AuthoringScopeId -eq "GLOBAL") {
                    $global = $true
                    $SettingLogicalName = "$($gcLogicalName)_Setting_LogicalName"
                } else {
                    $SettingLogicalName = $settingsxml.DesiredConfigurationDigest.GlobalSettings.Settings.FirstChild.FirstChild.LogicalName
                }
                if (!($SettingLogicalName)) {
                    $SettingLogicalName = $settingsxml.DesiredConfigurationDigest.GlobalExpression.LogicalName
                }

                #Checking for ConfigurationItemSetting 

                if (($null -eq $SettingSourceType) -AND ($SettingSourceType -ne "")) {
                    $CISettingSourceType = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ConfigurationItemSettingSourceType]::$SettingSourceType
                } else {
                    $CISettingSourceType = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ConfigurationItemSettingSourceType]::CIM
                }

                #if ($global){
                $arg = @($gcScope,
                    $gcLogicalName
                    $gcExpressionDataType,
                    $SettingLogicalName,
                    $CISettingSourceType
                )
                $reqSetting = New-Object -TypeName Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.GlobalSettingReference -ArgumentList $arg

                #custom properties Existential
                if ($operator.ToLower() -eq "notexistential") {
                    $operator = "Equals"
                    $Value = 0
                    $reqSetting.MethodType = "Count"
                }
                if ($operator.ToLower() -eq "existential") {
                    $operator = "NotEquals"
                    $Value = 0
                    $reqSetting.MethodType = "Count"
                }

                $arg = @($value,
                    $gcExpressionDataType
                )
                $reqValue = New-Object -TypeName Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.ConstantValue -ArgumentList $arg

                $operands = New-Object -TypeName 'Microsoft.ConfigurationManagement.DesiredConfigurationManagement.CustomCollection`1[[Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.ExpressionBase]]'
                $null = $operands.Add($reqSetting)
                $null = $operands.Add($reqValue)

                #Changing Equals to IsEquals 
                if ($operator.ToLower() -eq "equals") { $operator = "IsEquals" }
                $ExpOperator = [Microsoft.ConfigurationManagement.DesiredConfigurationManagement.ExpressionOperators.ExpressionOperator]::$operator

                $arg = @($ExpOperator,
                    $operands
                )
                $expression = New-Object -TypeName Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Expressions.Expression -ArgumentList $arg
                $expression.Operands[0].SettingLogicalName = $ConfigurationItemSettings.Setting_UniqueID

            }
            #>
        }


        # Define the Requirements Rule
        $RuleId = "DTRule_$([guid]::NewGuid())"
        $Annotation = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.Annotation]::new()

        # Format Requirement Rule name
        if ($Rule.DisplayName) {
            $DisplayNameString = $Rule.DisplayName
        } else {
            # Split the Operator into seperate words
            # Reference: https://social.technet.microsoft.com/Forums/ie/en-US/2c042285-7dcb-4126-8ee2-a297a8b7de6f/split-strings-with-capital-letters-and-numbers?forum=winserverpowershell            
            $OperatorString = $RequirementOperator.ToString().Trim()
            $OperatorString = ($OperatorString.substring(0, 1).toupper() + $OperatorString.substring(1) -creplace '[^\p{Ll}\s]', ' $&').Trim()

            $DisplayNameString = "$($Rule.Type) " + "$OperatorString " + "{$($RuleNameList -join ', ')}"            
        }

        $DisplayName = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.LocalizableString]::new('DisplayName', $DisplayNameString, $null)
        $Annotation.DisplayName = $DisplayName

        # Create the Requirement Rule
        $NewRequirementRule = [Microsoft.SystemsManagementServer.DesiredConfigurationManagement.Rules.Rule]::new($RuleId, $NoncomplianceSeverity, $Annotation, $Expression)

        $null = $RequirementsObjectList.Add($NewRequirementRule)   
    }

    Write-Host "END $CmdletName" -ForegroundColor Green
    $RequirementsObjectList
}