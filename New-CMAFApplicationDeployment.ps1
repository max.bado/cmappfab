﻿function New-CMAFApplicationDeployment {    
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        [string[]]$ApplicationName,

        [Parameter(Mandatory = $true)]
        [string[]]$CollectionName,

        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$ApplyToSubTargets,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$AppModelID,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [string]$AssignedCI_UniqueID,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int[]]$AssignedCIs,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$AssignmentAction,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [string]$AssignmentDescription,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [string]$AssignmentName,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$AssignmentType,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$ContainsExpiredUpdates,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [datetime]$CreationTime,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$DesiredConfigType,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$DisableMomAlerts,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$DPLocality,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$Enabled,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [datetime]$EnforcementDeadline,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [string]$EvaluationSchedule,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [datetime]$ExpirationTime,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$LocaleID,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$LogComplianceToWinEvent,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [int]$NonComplianceCriticality,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$NotifyUser,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$OfferFlags,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$OfferTypeID,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$OverrideServiceWindows,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$PersistOnWriteFilterDevices,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$Priority,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$RaiseMomAlertsOnFailure,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$RebootOutsideOfServiceWindows,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$RequireApproval,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$SendDetailedNonComplianceStatus,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$SoftDeadlineEnabled,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [string]$SourceSite,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [datetime]$StartTime,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$StateMessagePriority,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [int]$SuppressReboot,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [string]$TargetCollectionID,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [datetime]$UpdateDeadline,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$UpdateSupersedence,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$UseGMTTimes,
        [Parameter(Mandatory = $false, ParameterSetName = "CustomParameters")]
        [boolean]$UserUIExperience,
        [Parameter(Mandatory = $true, ParameterSetName = "CustomParameters")]
        [boolean]$WoLEnabled,

        [Parameter(Mandatory = $true, ParameterSetName = "DefaultAvailableParameters")]
        [switch]$DefaultAvailableParameters,
        
        [Parameter(Mandatory = $true, ParameterSetName = "DefaultRequiredParameters")]
        [switch]$DefaultRequiredParameters,

        [Parameter(Mandatory = $false, ParameterSetName = "DefaultAvailableParameters")]
        [Parameter(Mandatory = $false, ParameterSetName = "DefaultRequiredParameters")]
        [ValidateSet("Install", "Uninstall")]
        [string]$DeployAction = 'Install',

        [Parameter(Mandatory = $false, ParameterSetName = "DefaultAvailableParameters")]
        [Parameter(Mandatory = $false, ParameterSetName = "DefaultRequiredParameters")]
        [Parameter(Mandatory = $false)]
        [switch]$DisallowRepair
    )

    $Local:CmdletName = $PSCmdlet.MyInvocation.InvocationName
    Write-Host "START $CmdletName" -ForegroundColor Green

    $AdminConsoleDirectory = Get-Item -Path "$($ENV:SMS_ADMIN_UI_PATH)\.."

    $CurrentAssemblyList = [System.AppDomain]::CurrentDomain.GetAssemblies()

    # Load System.Windows.Forms.dll since it is not loaded by default by PowerShell Core
    if('System.Windows.Forms.dll' -notin $CurrentAssemblyList.ManifestModule.Name) {
        Write-Host "Loading System.Windows.Forms.dll"   
        $TargetAssemblyPath = Get-Item -Path "C:\Windows\Microsoft.NET\Framework64\*\System.Windows.Forms.dll" | Select-Object -Last 1
        $null = [Reflection.Assembly]::LoadFrom($TargetAssemblyPath.FullName)
    }

    # Load Configuration Manager Assemblies
    $filesToLoad = "Microsoft.ConfigurationManagement.ApplicationManagement.dll", 
        "AdminUI.WqlQueryEngine.dll", 
        "AdminUI.DcmObjectWrapper.dll", 
        "DcmObjectModel.dll", 
        "AdminUI.AppManFoundation.dll", 
        "AdminUI.WqlQueryEngine.dll", 
        "Microsoft.ConfigurationManagement.ApplicationManagement.Extender.dll", 
        "Microsoft.ConfigurationManagement.ManagementProvider.dll", 
        "Microsoft.ConfigurationManagement.ApplicationManagement.MsiInstaller.dll"
    
    Get-ChildItem -Path $AdminConsoleDirectory.FullName | Where-Object {($_.Name -in $filesToLoad) -and ($_.Name -notin $CurrentAssemblyList.ManifestModule.Name)} | ForEach-Object {
        $Module = $_
        Write-Host "Loading $($Module.Name)"    
        $null = [Reflection.Assembly]::LoadFrom($_.FullName)
    }

    # If CMServer or CMSite were not specified, retrieve the info from the Microsoft.SMS.Client object 
    if (!($CMServer) -or !($CMSite)) {
        $SMSClientObject = New-Object -ComObject Microsoft.SMS.Client -Strict
        if (!$CMServer) {
            $CMServer = $SMSClientObject.GetCurrentManagementPoint()
        }
        if (!$CMSite) {
            $CMSite = $SMSClientObject.GetAssignedSite()
        }
    }

    # Initialize connection manager and connect
    if(!$ConnectionManager) {
        $ConnectionManager = [Microsoft.ConfigurationManagement.ManagementProvider.WqlQueryEngine.WqlConnectionManager]::new()
        $null = $ConnectionManager.Connect($CMServer)
    }

    # Get list ofApplications
    if(!$CMAppList) {
        $CMAppListQuery = "SELECT * FROM SMS_Application
            WHERE SMS_Application.IsLatest = 'True' 
            AND SMS_Application.IsEnabled = 'True' 
            AND SMS_Application.IsExpired = 'False'"

        $CMAppList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query $CMAppListQuery
    }

    # Retrieve Distribution Point info
    if(!$DistributionPoint) {
        $DistributionPoint = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_DistributionPointInfo WHERE Name IN ( '$CMServer' )"
    }

    # Retrieve list of Collections
    if(!$CollectionList) {
        $CollectionList = Get-CimInstance -ComputerName $CMServer -Namespace "root\sms\site_$CMSite" -Query "SELECT * FROM SMS_Collection"
    }

    foreach($Application in $ApplicationName) {
        foreach ($Collection in $CollectionName) {
            Write-Host "Start creating '$Application' deployment to '$Collection'" -ForegroundColor Yellow
            # Get Collection info
            $TargetCollection = $CollectionList | Where-Object {$_.Name -eq $Collection}
    
            # Get Application info
            $CMTargetApp = $CMAppList | Where-Object {$_.LocalizedDisplayName -eq $Application}

            # Initialize Application Assignment object to populate with properties
            $NewApplicationAssignment = $ConnectionManager.CreateInstance("SMS_ApplicationAssignment")        

            if($PSCmdlet.ParameterSetName -ne 'CustomParameters') {
        
                $CreationTime = Get-Date

                # Convert Deploy Action into corresponding DesiredConfigType int value
                switch ($DeployAction) {
                    'Install' {$DesiredConfigType = 1}
                    'Uninstall' {$DesiredConfigType = 2}
                }

                # Set Required/Available specific parameters
                switch ($PSCmdlet.ParameterSetName) {
                    'DefaultAvailableParameters' {
                        $OfferTypeID = 2                
                    }

                    'DefaultRequiredParameters' {
                        $OfferTypeID = 0
                        $NewApplicationAssignment['EnforcementDeadline'].DateTimeValue = $CreationTime
                    }
                }

                $NewApplicationAssignment['ApplicationName'].StringValue = $Application
                $NewApplicationAssignment['AppModelID'].IntegerValue = $CMTargetApp.CI_ID
                $NewApplicationAssignment['AssignedCI_UniqueID'].StringValue = $CMTargetApp.CI_UniqueID
                $NewApplicationAssignment['AssignedCIs'].IntegerArrayValue = @($CMTargetApp.CI_ID)
                $NewApplicationAssignment['AssignmentAction'].IntegerValue = 2
                #$NewApplicationAssignment.AssignmentID
                $NewApplicationAssignment['AssignmentName'].StringValue = "$($CMTargetApp.LocalizedDisplayName)_$($TargetCollection.Name)_$($DeployAction)"
                $NewApplicationAssignment['AssignmentType'].IntegerValue = 2
                #$NewApplicationAssignment.AssignmentUniqueID
                $NewApplicationAssignment['CollectionName'].StringValue = "$($TargetCollection.Name)"
                $NewApplicationAssignment['CreationTime'].DateTimeValue = $CreationTime        
                $NewApplicationAssignment['DesiredConfigType'].IntegerValue = $DesiredConfigType        
                $NewApplicationAssignment['LocaleID'].IntegerValue = 1033
                $NewApplicationAssignment['NotifyUser'].BooleanValue = $false
                $NewApplicationAssignment['OfferFlags'].IntegerValue = 0
                $NewApplicationAssignment['OfferTypeID'].IntegerValue = $OfferTypeID
                $NewApplicationAssignment['OverrideServiceWindows'].BooleanValue = $true
                $NewApplicationAssignment['PersistOnWriteFilterDevices'].BooleanValue = $false
                $NewApplicationAssignment['RebootOutsideOfServiceWindows'].BooleanValue = $false
                $NewApplicationAssignment['RequireApproval'].BooleanValue = $false
                $NewApplicationAssignment['SourceSite'].StringValue = $CMSite
                $NewApplicationAssignment['StartTime'].DateTimeValue = $CreationTime
                $NewApplicationAssignment['SuppressReboot'].IntegerValue = 0
                $NewApplicationAssignment['TargetCollectionID'].StringValue = $TargetCollection.CollectionID
                $NewApplicationAssignment['UseGMTTimes'].BooleanValue = $false
                $NewApplicationAssignment['UserUIExperience'].BooleanValue = $true
                $NewApplicationAssignment['WoLEnabled'].BooleanValue = $false

                if(!$DisallowRepair) {
                    if(($NewApplicationAssignment["OfferFlags"].IntegerValue -lt 8) -or (($NewApplicationAssignment["OfferFlags"].IntegerValue -ge 16) -and ($NewApplicationAssignment["OfferFlags"].IntegerValue -le 23))) {
                        $NewApplicationAssignment["OfferFlags"].IntegerValue += 8
                    }                
                } else {
                    if((($NewApplicationAssignment["OfferFlags"].IntegerValue -gt 8) -and ($NewApplicationAssignment["OfferFlags"].IntegerValue -lt 16)) -or ($NewApplicationAssignment["OfferFlags"].IntegerValue -ge 24)) {
                        $NewApplicationAssignment["OfferFlags"].IntegerValue -= 8
                    }
                }    

            } else {
        
                foreach($Parameter in $PSCmdlet.MyInvocation.BoundParameters.Keys) {
                    $Value = $PSCmdlet.MyInvocation.BoundParameters.Item($Parameter)
                    $Type = $null
                    switch ($Value.GetType()) {
                        'bool' {$Type = 'BooleanValue'}
                        'string' {$Type = 'StringValue'}
                        'string[]' {$Type = 'StringValue'}
                        'int' {$Type = 'IntegerValue'}
                        'int[]' {$Type = 'IntegerArrayValue'}
                        'datetime' {$Type = 'DateTimeValue'}
                    }

                    $NewApplicationAssignment[$Parameter].$Type = $Value

                }
            }


            try {
                [void]$NewApplicationAssignment.Put()
                Write-Host "Finish creating '$Application' deployment to '$Collection'" -ForegroundColor Yellow
            } catch {
                $Err = $_
                Write-Error $Err
            }
        }
    
    }

    Write-Host "END $CmdletName" -ForegroundColor Green
}